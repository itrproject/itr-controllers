/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.functions;

import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class CheckUserITMappingExistence {

    private int UserID;
    private Session session;
    public CheckUserITMappingExistence(int UserID,Session session) {
        this.UserID = UserID;
        this.session=session;
    }
    
    public Useritmapping CheckUser(){
       
            //create object of userITMapping
            Useritmapping useritmapping = new Useritmapping();

            //create object of userITMapping Dao
            userITMappingDao userITMappingDaoObject = new userITMappingDao(session);

            //check if userITMapping of user already exists or not
            useritmapping = userITMappingDaoObject.getUsingUID(UserID);
            
           
            //--------------Case 1: If user's IT mapping does not already exists
            
            //Action1: Insert userITMapping object
            //Action2: Insert Deduction object
            //Action3: Insert BroughtForwardLosses Object
            if (useritmapping == null) {
                System.out.println("New");
            //fetch the user object from user table
                ///------------------------------Object of user master for insertion-----------------///
                
                //set user master object with its ID to retrieve that object from user master
                UserMaster userMaster = new UserMaster();
                userMaster.setUId(UserID);
                    
                //store userMaster object for the the mapping of the user
                UserMasterDao userMasterDao = new UserMasterDao(session);
                userMaster = userMasterDao.get(userMaster);
                
            ///---------------------Object of userITMapping for insertion-------------------------///
                //set object of useritmapping to insert it into database
                useritmapping=new Useritmapping();
                useritmapping.setUserMaster(userMaster);
               
                //save the record of userITMapping in the database and Retrieve ID of the lastly inserted userIT Mapping
                userITMappingDaoObject.save(useritmapping);
                
            }
            else{
                System.out.println("Existing");
            }
            return useritmapping;
                    
    }
}
