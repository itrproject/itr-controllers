/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.objects;

/**
 *
 * @author Administrator
 */
public class Exemption{

    private int ExemptionTypeID;
    private int ExemptionAmount;
    public Exemption() {
    }

    public Exemption(int ExemptionTypeID, int ExemptionAmount) {
        this.ExemptionTypeID = ExemptionTypeID;
        this.ExemptionAmount = ExemptionAmount;
    }

    public int getExemptionTypeID() {
        return ExemptionTypeID;
    }

    public void setExemptionTypeID(int ExemptionTypeID) {
        this.ExemptionTypeID = ExemptionTypeID;
    }

    public int getExemptionAmount() {
        return ExemptionAmount;
    }

    public void setExemptionAmount(int ExemptionAmount) {
        this.ExemptionAmount = ExemptionAmount;
    }
    
}
