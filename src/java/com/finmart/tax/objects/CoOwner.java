/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.objects;

/**
 *
 * @author Administrator
 */
public class CoOwner {

    private String Name;
    private String PAN;
    private int OwnershipPercentage;
    public CoOwner() {
    }

    public CoOwner(String Name, String PAN, int OwnershipPercentage) {
        this.Name = Name;
        this.PAN = PAN;
        this.OwnershipPercentage = OwnershipPercentage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPAN() {
        return PAN;
    }

    public void setPAN(String PAN) {
        this.PAN = PAN;
    }

    public int getOwnershipPercentage() {
        return OwnershipPercentage;
    }

    public void setOwnershipPercentage(int OwnershipPercentage) {
        this.OwnershipPercentage = OwnershipPercentage;
    }
   
}
