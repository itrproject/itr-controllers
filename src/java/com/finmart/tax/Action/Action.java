/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finmart.tax.Action;

import java.util.List;

/**
 *
 * @author admin
 */
public interface Action {
    public List<String> performAction();
    //public List<String> errors();
}
