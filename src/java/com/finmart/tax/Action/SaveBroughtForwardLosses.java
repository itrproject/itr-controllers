/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.BroughtForwardLossesWrapper;
import com.finmart.tax.dao.Broughtforwardlosses;
import com.finmart.tax.dao.Deductions;
import com.finmart.tax.dao.Deductiontypes;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.BroughtForwardLossesDao;
import com.finmart.tax.dao.action.DeductionTypeDao;
import com.finmart.tax.dao.action.DeductionsDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveBroughtForwardLosses implements Action{
    private int UserID;
    private BroughtForwardLossesWrapper broughtForwardLossesWrapper;
    private Session session;
    private Integer DeductionTypeID=3;
    public SaveBroughtForwardLosses(BroughtForwardLossesWrapper broughtForwardLossesWrapper, Session session) {
        this.broughtForwardLossesWrapper = broughtForwardLossesWrapper;
        this.session = session;
    }
    
    

    @Override
    public List<String> performAction() {
        List<String> errors=null;
         try {
            //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
             //fetch the user ID
             UserID = broughtForwardLossesWrapper.getUserID();
             CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();
             
             
            //Action1: Insert Deduction object
            //Action2: Insert BroughtForwardLosses Object
            
            ///-----------------Object of DeductionType type to be inserted------------------------///
                //Object of Dao of Deduction type
                DeductionTypeDao deductionTypeDao = new DeductionTypeDao(session);
                
                
                //Object of DeductionType to retrieve object of DeductionType
                Deductiontypes deductionTypes = new Deductiontypes();
                deductionTypes.setDeductionTypesId(DeductionTypeID);
                deductionTypes = deductionTypeDao.get(deductionTypes);
                
                
            ///---------------Object of Deductions for insertion in the income table-------------///
                

                
                //Create object of Deductions to save it to income table
                Deductions deductions = new Deductions();
                deductions.setUseritmapping(useritmapping);
                deductions.setDeductiontypes(deductionTypes);
                
                
                //Create object of Deducitons to save the Deductions object and Retrieve ID of the lastly inserted income ID
                DeductionsDao deductionsDao = new DeductionsDao(session);
                deductionsDao.save(deductions);
                
                 
                                                                                
             
                
             ///----------------Object of BroughtForwardLosses to be inserted in the database-----------------///
                //Object of BroughtForwardLosses
                Broughtforwardlosses broughtForwardLosses = new Broughtforwardlosses();
                broughtForwardLosses.setAssessmentYear(broughtForwardLossesWrapper.getAssessmentYear());
                broughtForwardLosses.setDateOfFiling(broughtForwardLossesWrapper.getDateOfFiling());
                broughtForwardLosses.setDeductions(deductions);
                broughtForwardLosses.setLongTermCapitalLoss(broughtForwardLossesWrapper.getLongTermCapitalLoss());
                broughtForwardLosses.setLossFromHouseProperty(broughtForwardLossesWrapper.getLossFromHouseProperty());
                broughtForwardLosses.setOtherSourceLoss(broughtForwardLossesWrapper.getOtherSourceLoss());
                broughtForwardLosses.setShortTermCapitalLoss(broughtForwardLossesWrapper.getShortTermCapitalLoss());
                
                
                //Object of BroughtForwardLosses Dao
                BroughtForwardLossesDao broughtForawardLossesDao = new BroughtForwardLossesDao(session);
                
                
                //save the BroughtForwardLosses for the user
                broughtForawardLossesDao.save(broughtForwardLosses);
                
                
        
            

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
}