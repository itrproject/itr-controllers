/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action.Servlets;

import com.finmart.hibernateUtil.NewHibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class HibernateAction {
    public static Session beginTransation() {
        Session session;
        session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        return session;
    }
    public static void commit(Session session) {
        session.getTransaction().commit();
       // session.flush();
        NewHibernateUtil.shutdown();
    }
}
