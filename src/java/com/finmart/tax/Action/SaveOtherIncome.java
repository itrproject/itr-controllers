/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.OtherIncomesWrapper;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Otherincomes;
import com.finmart.tax.dao.Otherincometypes;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.OtherIncomeTypesDao;
import com.finmart.tax.dao.action.OtherIncomesDao;
import com.finmart.tax.dao.action.SalaryIncomeTypeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveOtherIncome implements Action{

    int UserID;
    Session session;
    OtherIncomesWrapper otherIncomesWrapper;
    private int OtherIncomeType;
    private int IncomeTypeID=5;
    public SaveOtherIncome(OtherIncomesWrapper otherIncomesWrapper,Session session) {
        this.otherIncomesWrapper = otherIncomesWrapper;
        this.session=session;
    }
    @Override
    public List<String> performAction() {
        
        List<String> errors=null;
         try {
             //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = otherIncomesWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

           
              ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);
                
               
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
                
                                                     
                
             ///----------------Object of OtherIncomeType to be inserted in the database-----------------///
              
               //Object of Dao of OtherIncometype
               OtherIncomeTypesDao otherIncomeTypeDao = new OtherIncomeTypesDao(session);
                
                OtherIncomeType = otherIncomesWrapper.getIdofOtherIncome();
                //Object of OtherIncomeType to retrieve object of OtherIncomeType
                Otherincometypes otherIncomeType = new Otherincometypes();
                otherIncomeType.setOtherIncomeTypesId(OtherIncomeType);
                
               
                otherIncomeType = otherIncomeTypeDao.get(otherIncomeType);
                
                
          ///----------------Object of OtherIncome to be inserted in the database-----------------///
                //Object of OtherIncome
                Otherincomes otherIncomes = new Otherincomes();
                otherIncomes.setIncome(income);
                otherIncomes.setOtherincometypes(otherIncomeType);
                otherIncomes.setAmountOfIncome(otherIncomesWrapper.getAmountOfIncome());
                
           
                
                
                //Object of OtherIncomeDao
                OtherIncomesDao otherIncomesDao = new OtherIncomesDao(session);
         
                
                
                //save the OtherIncome for the user
                otherIncomesDao.save(otherIncomes);
                
                
                
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
                
    }
            
}
