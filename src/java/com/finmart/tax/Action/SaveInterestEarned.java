/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.InterestEarnedWrapper;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Interestearned;
import com.finmart.tax.dao.Interesttype;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.InterestIncomeDao;
import com.finmart.tax.dao.action.InterestTypeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.classic.Session;

/**
 *
 * @author Administrator
 */
public class SaveInterestEarned implements Action{
    int UserID;
    int IncomeTypeID=3;
    Session session;
    InterestEarnedWrapper interestEarnedWrapper;
    private int InterestTypeID;
    public SaveInterestEarned() {
    }

    public SaveInterestEarned(InterestEarnedWrapper interestEarnedWrapper,Session session) {
        this.interestEarnedWrapper=interestEarnedWrapper;
        this.session=session;
    }
    
    @Override
    public List<String> performAction() {
        List<String> errors=null;
          try {
              //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = interestEarnedWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);
                
               
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
                                                 
            
              ///----------------Object of InterestIncomeType to be inserted in the database-----------------///
               InterestTypeID=interestEarnedWrapper.getInterestIncomeTypeID();
               //Object of Dao of Interesttype
               InterestTypeDao interestTypeDao = new InterestTypeDao(session);
                
                
                //Object of InterestType to retrieve object of SalaryIncomeType
                Interesttype interestType = new Interesttype();
                interestType.setInterestTypeId(InterestTypeID);
                
               
                interestType = interestTypeDao.get(interestType);
                
             ///----------------Object of interest income to be inserted in the database-----------------///
                //Object of InterestIncome
                Interestearned interestEarned = new Interestearned();
                interestEarned.setIncome(income);
                interestEarned.setInterestEarnedId(interestEarnedWrapper.getInterestIncomeTypeID());
                interestEarned.setInterestEarned(interestEarnedWrapper.getInterestIncomeValue());
                interestEarned.setInterestFrom(interestEarnedWrapper.getInterestFrom());
                interestEarned.setInteresttype(interestType);
                
                
                //Object of Interestearned Dao
                InterestIncomeDao interestIncomeDao = new InterestIncomeDao(session);
         
                
                
                //save the tax Exempt income for the user
                interestIncomeDao.save(interestEarned);
                
                
                
             
                
                
              
        } catch (Exception e) {
       //     errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
    
}
