/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.SaveEmployerWrapper;
import com.finmart.tax.dao.CityMaster;
import com.finmart.tax.dao.Employer;
import com.finmart.tax.dao.Employertype;
import com.finmart.tax.dao.Salaryincome;
import com.finmart.tax.dao.action.CityMasterDao;
import com.finmart.tax.dao.action.EmployerDao;
import com.finmart.tax.dao.action.EmployerTypeDao;
import com.finmart.tax.dao.action.SalaryIncomeDao;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveEmployerInfo implements Action{

   

    SaveEmployerWrapper saveEmployerWrapper;
    Session session;
    private int SaveEmployerType;
    private int CityID;
    public SaveEmployerInfo(SaveEmployerWrapper saveEmployerWrapper, Session session) {
        this.saveEmployerWrapper = saveEmployerWrapper;
        this.session = session;
    }
    @Override
    public List<String> performAction() {
        List<String> errors=null;
          try {
          
              ///-----------------Object of Employer type to be inserted------------------------///
                SaveEmployerType = saveEmployerWrapper.getEmployerTypeID();
                //Object of Dao of Employer type
                EmployerTypeDao employerTypeDao = new EmployerTypeDao(session);
                
                
                //Object of employer type to retrieve object of EmployerType
                Employertype employerType = new Employertype();
                employerType.setEmptypeId(SaveEmployerType);
                
               
                employerType = employerTypeDao.get(employerType);
                
                
             ///-----------------Object of City Master to be inserted------------------------///
                CityID=saveEmployerWrapper.getEmployerCity();
                
                //Object of Dao of City Master
                CityMasterDao cityMasterDao = new CityMasterDao(session);
                
                
                //Object of CityMaster to retrieve object of CityMaster
                CityMaster cityMaster = new CityMaster();
                cityMaster.setCityId(CityID);
                
               
                cityMaster = cityMasterDao.get(cityMaster);
                
                
            ///----------------Object of employer to be inserted in the database-------------------///
                
                Employer employer = new Employer();
                employer.setEmployerName(saveEmployerWrapper.getEmployerName());
                employer.setEmployerAddress(saveEmployerWrapper.getEmployerAddress());
                employer.setEmployerTanNo(saveEmployerWrapper.getEmployerTANNo());
                employer.setEmployertype(employerType);
                employer.setEmploymentPeriod(saveEmployerWrapper.getEmploymentPeriod());
                employer.setCityMaster(cityMaster);
              
                
                EmployerDao employerDao = new EmployerDao(session);
                
                
                
                //save the object of employer
                employerDao.save(employer);
         
          //----------------------Object of salaryIncome to be edited---------------------/
                //Object of SalaryIncome to retrieve the actual object of Salary Income
                Salaryincome salaryIncome = new Salaryincome();
                salaryIncome.setSalIncId(saveEmployerWrapper.getSalaryIncomeID());
                
                
                //Object of SalaryIncome Dao
                SalaryIncomeDao salaryIncomeDao = new SalaryIncomeDao(session);
                
                
                //Retrieve the actual object of SalaryIncome and update it with employer details
                salaryIncome=salaryIncomeDao.get(salaryIncome);              
                salaryIncome.setEmployer(employer);
                
                //update the salaryIncome Object
                salaryIncomeDao.update(salaryIncome);
                
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
    
}
