/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.DonationWrapper;
import com.finmart.tax.dao.Deductions;
import com.finmart.tax.dao.Deductiontypes;
import com.finmart.tax.dao.Donations;
import com.finmart.tax.dao.Donationtypes;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.DeductionTypeDao;
import com.finmart.tax.dao.action.DeductionsDao;
import com.finmart.tax.dao.action.DonationTypesDao;
import com.finmart.tax.dao.action.DonationsDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveDonation implements Action{
    private Integer DeductionTypeID=1;
    private int UserID;
    DonationWrapper donationWrapper;
    Session session;
    
    public SaveDonation(DonationWrapper donationWrapper, Session session) {
        this.donationWrapper = donationWrapper;
        this.session = session;
    }
    
    @Override
    public List<String> performAction() {
        List<String> errors=null;
         try {
             //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
             //fetch the user ID
            //fetch the user ID
            UserID = donationWrapper.getUserID();

            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();
            
             ///-----------------Object of DeductionType type to be inserted------------------------///
                //Object of Dao of Deduction type
                DeductionTypeDao deductionTypeDao = new DeductionTypeDao(session);
                
                
                //Object of DeductionType to retrieve object of DeductionType
                Deductiontypes deductionTypes = new Deductiontypes();
                deductionTypes.setDeductionTypesId(DeductionTypeID);
                deductionTypes = deductionTypeDao.get(deductionTypes);
                
                
            ///---------------Object of Deductions for insertion in the income table-------------///
                

                
                //Create object of Deductions to save it to income table
                Deductions deductions = new Deductions();
                deductions.setUseritmapping(useritmapping);
                deductions.setDeductiontypes(deductionTypes);
                
                
                //Create object of Deducitons to save the Deductions object and Retrieve ID of the lastly inserted income ID
                DeductionsDao deductionsDao = new DeductionsDao(session);
               deductionsDao.save(deductions);
                
               
                                                                                
              ///-----------------Object of DonationTypeDao to be inserted------------------------///
                //Object of Dao of DonationType
                DonationTypesDao donationTypesDao = new DonationTypesDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Donationtypes donationTypes = new Donationtypes();
                donationTypes.setDonationTypeId(donationWrapper.getDonationTypeID());
                donationTypes = donationTypesDao.get(donationTypes);
                
                
             ///----------------Object of Donations to be inserted in the database-----------------///
                //Object of Donation
                Donations donation = new Donations();
                donation.setAddressOfDonee(donationWrapper.getAddressOfDonee());
                donation.setDeductionAmount(donationWrapper.getDeductionAmount());
                donation.setDeductions(deductions);
                donation.setDonatedAmount(donationWrapper.getDonatedAmount());
                donation.setDonationtypes(donationTypes);
                donation.setNameOfDonee(donationWrapper.getNameOfDonee());
                donation.setPanofDonee(donationWrapper.getPanOfDonee());
                
                
                //Object of Doantion Dao
                DonationsDao donationsDao = new DonationsDao(session);
                
                
                //save the Donaitons for the user
                donationsDao.save(donation);
                

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
   
    
}
