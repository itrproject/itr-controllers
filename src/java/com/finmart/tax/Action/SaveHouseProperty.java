/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.objects.Exemption;
import com.finmart.tax.objects.CoOwner;
import com.finmart.tax.beans.wrapper.HousePropertyWrapper;
import com.finmart.tax.dao.Capitalgain;
import com.finmart.tax.dao.Capitalgaintype;
import com.finmart.tax.dao.CoOwners;
import com.finmart.tax.dao.Exemptionfromcapitalgain;
import com.finmart.tax.dao.Exemptions;
import com.finmart.tax.dao.Exemptiontype;
import com.finmart.tax.dao.Houseproperty;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.CapitalGainDao;
import com.finmart.tax.dao.action.CapitalGainTypeDao;
import com.finmart.tax.dao.action.CoOwnersDao;
import com.finmart.tax.dao.action.ExemptionFromCapitalGainDao;
import com.finmart.tax.dao.action.ExemptionTypeDao;
import com.finmart.tax.dao.action.ExemptionsDao;
import com.finmart.tax.dao.action.HousePropertyDao;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveHouseProperty implements Action{
    private int UserID;
    private Integer IncomeTypeID=6;
    private Integer CapitalGainTypeID=4;

    public SaveHouseProperty(HousePropertyWrapper housePropertyWrapper, Session session) {
        this.housePropertyWrapper = housePropertyWrapper;
        this.session = session;
    }

    HousePropertyWrapper housePropertyWrapper;
    Session session;
    @Override
    public List<String> performAction() {
        List<String> errors=null;
        try{
        
            //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = housePropertyWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();


        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }

    private void saveHouseProperty(Useritmapping useritmapping) {
        ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);
                
               
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
                
                                                      
            
              ///----------------Object of CapitalGainType to be inserted in the database-----------------///
               
               //Object of Dao of CapitalGainType
               CapitalGainTypeDao capitalGainTypeDao = new CapitalGainTypeDao(session);
                
                
                //Object of CapitalGainType to retrieve object of CapitalGainType
                Capitalgaintype capitalGainType = new Capitalgaintype();
                capitalGainType.setCapitalGainTypeId(CapitalGainTypeID);
                
               
                capitalGainType = capitalGainTypeDao.get(capitalGainType);
                
             ///----------------Object of CapitalGain to be inserted in the database-----------------///
                //Object of CapitalGain
                Capitalgain capitalGain = new Capitalgain();
                capitalGain.setCapitalGainTerm(housePropertyWrapper.getCapitalGainTerm());
                capitalGain.setCapitalgaintype(capitalGainType);
                capitalGain.setIncome(income);
                
                
                //Object of CapitalGain Dao
                CapitalGainDao capitalGainDao = new CapitalGainDao(session);
         
                
                
                //save the CapitalGain for the user
                capitalGainDao.save(capitalGain);
                
               
                
             ///----------------Object of HouseProperty to be inserted in the database-----------------///
                //Object of HouseProperty
                Houseproperty houseProperty = new Houseproperty();
                houseProperty.setAddressOfProperty(housePropertyWrapper.getAddressOfProperty());
                houseProperty.setCapitalgain(capitalGain);
                houseProperty.setCostDate(housePropertyWrapper.getCostDate());
                houseProperty.setCostOfImprovement(housePropertyWrapper.getCostOfImprovement());
                houseProperty.setCostValue(housePropertyWrapper.getCostValue());
                houseProperty.setDateOfImprovement(housePropertyWrapper.getDateOfImprovement());
                houseProperty.setExpenseRelatedToSale(housePropertyWrapper.getExpenseRelatedToSale());
                houseProperty.setPropertyOwnershipPercent(housePropertyWrapper.getOwnershipPercentage());
                houseProperty.setSaleDate(housePropertyWrapper.getSaleDate());
                houseProperty.setSaleValue(housePropertyWrapper.getSaleValue());
                houseProperty.setTypeOfProperty(housePropertyWrapper.getTypeOfProperty());
                houseProperty.setUseOfProperty(housePropertyWrapper.getUseOfProperty());
                
                
                HousePropertyDao housePropertyDao = new HousePropertyDao(session);
                
                housePropertyDao.save(houseProperty);
               
                   
                
            ///----------------Objects of ExemptionFromCapitalGain to be inserted in the database-------------///  
                if(housePropertyWrapper.getHouseExemptions().size()>0){
                    ///--------------------Object of ExemptionTypeDao--------------------------///
                    ExemptionTypeDao exemptionTypeDao = new ExemptionTypeDao(session);
                    //Object of ExemptionType
                    Exemptiontype exemptionType;
                    
                    
                    //Object of Exemptions Dao
                    ExemptionsDao exemptionsDao = new ExemptionsDao(session);
                    //Object of Exemptions
                    Exemptions exemptions;
                    
                    
                    
                    //Object of ExemptionFromCapitalGain Dao
                    ExemptionFromCapitalGainDao exemptionFromCapitalGainDao = new ExemptionFromCapitalGainDao(session);
                    //Object of ExemptionFromCapitalGain
                    Exemptionfromcapitalgain exemptionFromCapitalGain = new Exemptionfromcapitalgain();
                    
                    
                    Exemption exemption;
                    
                    
                    //Saving all the exemptions for PlotOfLand
                    for(int i=0;i<housePropertyWrapper.getHouseExemptions().size();i++){
                        
                        //Object to retrieve the exemption details
                        exemption = housePropertyWrapper.getHouseExemptions().get(i);
                        
                        
                        //Object of exemptionType to be inserted
                        exemptionType = new Exemptiontype();
                        
                        exemptionType.setExemptionTypeId(exemption.getExemptionTypeID());                                                
                        exemptionType=exemptionTypeDao.get(exemptionType);
                        
                        //Object of Exemptions to be inserted in the database
                        exemptions = new Exemptions();
                        
                        exemptions.setExemptionAmount(exemption.getExemptionAmount());
                        exemptions.setExemptiontype(exemptionType);
                        
                        exemptionsDao.save(exemptions);
                       
                        
                        
                        
                        //Object of ExemptionFromCapitalGain
                        exemptionFromCapitalGain = new Exemptionfromcapitalgain();
                        
                        exemptionFromCapitalGain.setExemptions(exemptions);
                        exemptionFromCapitalGain.setHouseproperty(houseProperty);
                        
                        exemptionFromCapitalGainDao.save(exemptionFromCapitalGain);
                        
                    }
                    
                }
                // if the ownership percentage is less than 100 or has co-owners
                if(housePropertyWrapper.getOwnershipPercentage()<100){
                    //Object to store every co-owner
                    CoOwner coOwner;
                    
                    
                    //Object of CoOwners Dao
                    CoOwnersDao coOwnersDao = new CoOwnersDao(session);
                    
                            
                    //Object of CoOwners
                    CoOwners coOwners;
                    
                    for(int i=0;i<housePropertyWrapper.getCoOwner().size();i++){
                        coOwner = new CoOwner();
                        coOwner = housePropertyWrapper.getCoOwner().get(i);
                        
                        
                        
                        
                        //Object of CoOwners to be stored in database
                        coOwners = new CoOwners();
                        coOwners.setCoOwnerPan(coOwner.getPAN());
                        coOwners.setCoOwnersName(coOwner.getName());
                        coOwners.setHouseproperty(houseProperty);
                        
                        
                        coOwnersDao.save(coOwners);
                    }
                }
             
    }
    }
    

