/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;



import com.finmart.tax.beans.wrapper.SalaryIncomeWrapper;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Salaryincome;
import com.finmart.tax.dao.Salryincometype;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.SalaryIncomeDao;
import com.finmart.tax.dao.action.SalaryIncomeTypeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author admin
 */
public class SaveSalaryIncome implements Action{

    private int UserID;
    private SalaryIncomeWrapper salaryIncomeWrapper;
    private Session session;
    private Integer IncomeTypeID=4;
    private int SalaryIncomeType;
    
    public SaveSalaryIncome() {
    }
    
    public SaveSalaryIncome(SalaryIncomeWrapper salaryIncomeWrapper, Session session) {
        this.salaryIncomeWrapper = salaryIncomeWrapper;
        this.session = session;
    }


    @Override
    public List<String> performAction() {
         List<String> errors=null;
         try {
             //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = salaryIncomeWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);
                
               
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
                                                       
                
             ///----------------Object of SalaryIncomeType to be inserted in the database-----------------///
              SalaryIncomeType = salaryIncomeWrapper.getSalaryIncomeType();
               //Object of Dao of SalaryIncometype
               SalaryIncomeTypeDao salaryIncomeTypeDao = new SalaryIncomeTypeDao(session);
                
                
                //Object of SalaryIncomeType to retrieve object of SalaryIncomeType
                Salryincometype salaryIncomeType = new Salryincometype();
                salaryIncomeType.setSalaryIncomeTypeId(SalaryIncomeType);
                
               
                salaryIncomeType = salaryIncomeTypeDao.get(salaryIncomeType);
                
                
          ///----------------Object of SalaryIncome to be inserted in the database-----------------///
                //Object of SalaryIncome
                Salaryincome salaryIncome = new Salaryincome();
                salaryIncome.setEmployeeBasicSalary(salaryIncomeWrapper.getEmployeeBasicSalary());
                salaryIncome.setEmployeeGrossSalary(salaryIncomeWrapper.getEmployeeGrossSalary());
                salaryIncome.setEmployeeProfessionalTax(salaryIncomeWrapper.getEmployeeProfessionalTax());
                salaryIncome.setSalryincometype(salaryIncomeType);
                salaryIncome.setIncome(income);
           
                
                
                //Object of SalaryIncomeDao
                SalaryIncomeDao salaryIncomeDao = new SalaryIncomeDao(session);
         
                
                
                //save the SalaryIncome for the user
                int SalaryIncomeID=salaryIncomeDao.save(salaryIncome);
                
                
                
             
                
                

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
   
    }
    
    

    
  
    
    

