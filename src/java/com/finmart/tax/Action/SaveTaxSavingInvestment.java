/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.TaxSavingInvestmentWrapper;
import com.finmart.tax.dao.Deductions;
import com.finmart.tax.dao.Deductiontypes;
import com.finmart.tax.dao.Investmenttoolstypes;
import com.finmart.tax.dao.Taxsavinginvestments;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.DeductionTypeDao;
import com.finmart.tax.dao.action.DeductionsDao;
import com.finmart.tax.dao.action.InvestmentToolsTypesDao;
import com.finmart.tax.dao.action.TaxSavingInvestmentsDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveTaxSavingInvestment implements Action{
    private int UserID;
    private Integer DeductionTypeID=2;

    public SaveTaxSavingInvestment(TaxSavingInvestmentWrapper taxSavingInvestmentWrapper, Session session) {
        this.taxSavingInvestmentWrapper = taxSavingInvestmentWrapper;
        this.session = session;
    }

    TaxSavingInvestmentWrapper taxSavingInvestmentWrapper;
    Session session;
    @Override
    public List<String> performAction() {
         List<String> errors=null;
         try {
             //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = taxSavingInvestmentWrapper.getUserID();
             CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of DeductionType type to be inserted------------------------///
                //Object of Dao of Deduction type
                DeductionTypeDao deductionTypeDao = new DeductionTypeDao(session);
                
                
                //Object of DeductionType to retrieve object of DeductionType
                Deductiontypes deductionTypes = new Deductiontypes();
                deductionTypes.setDeductionTypesId(DeductionTypeID);
                deductionTypes = deductionTypeDao.get(deductionTypes);
                
                
            ///---------------Object of Deductions for insertion in the income table-------------///
                

                
                //Create object of Deductions to save it to income table
                Deductions deductions = new Deductions();
                deductions.setUseritmapping(useritmapping);
                deductions.setDeductiontypes(deductionTypes);
                
                
                //Create object of Deducitons to save the Deductions object and Retrieve ID of the lastly inserted income ID
                DeductionsDao deductionsDao = new DeductionsDao(session);
                deductionsDao.save(deductions);
                
                                                                                
              ///-----------------Object of InvestmentToolsTypes to be inserted------------------------///
                //Object of Dao of InvestmentToolsTypes
                InvestmentToolsTypesDao investmentToolsTypesDao = new InvestmentToolsTypesDao(session);
                
                
                //Object of InvestmentToolsTypes to retrieve object of InvestmentToolsTypes
                Investmenttoolstypes investmentToolsTypes = new Investmenttoolstypes();
                investmentToolsTypes.setInvestmentToolsTypesId(taxSavingInvestmentWrapper.getInvestementToolsTypesID());
                investmentToolsTypes = investmentToolsTypesDao.get(investmentToolsTypes);
                
                
             ///----------------Object of TaxSavingInvestment to be inserted in the database-----------------///
                //Object of TaxSavingInvestment
                Taxsavinginvestments taxSavingInvestments = new Taxsavinginvestments();
                taxSavingInvestments.setDeductions(deductions);
                taxSavingInvestments.setInvestmenttoolstypes(investmentToolsTypes);
                taxSavingInvestments.setInvestmentAmount(taxSavingInvestmentWrapper.getInvestmentAmount());
                
                
                
                //Object of TaxSavingInvestment Dao
                TaxSavingInvestmentsDao taxSavingInvestmentDao = new TaxSavingInvestmentsDao(session);
                
                
                //save the TaxSavingInvestment for the user
                taxSavingInvestmentDao.save(taxSavingInvestments);
                

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }

   
    
}
