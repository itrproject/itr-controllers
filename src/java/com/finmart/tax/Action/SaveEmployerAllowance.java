/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.EmployerAllowancesWrapper;

import com.finmart.tax.dao.Allowances;
import com.finmart.tax.dao.Employerallowance;
import com.finmart.tax.dao.Salaryincome;
import com.finmart.tax.dao.action.AllowancesDao;
import com.finmart.tax.dao.action.EmployerAllowanceDao;
import com.finmart.tax.dao.action.SalaryIncomeDao;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveEmployerAllowance implements Action{

   

    EmployerAllowancesWrapper employerAllowancesWrapper;
    Session session;
    private int SalaryIncomeID;
    private int EmployerAllowanceID;
     public SaveEmployerAllowance(EmployerAllowancesWrapper employerAllowancesWrapper, Session session) {
        this.employerAllowancesWrapper=employerAllowancesWrapper;
        this.session = session;
    }
    @Override
    public List<String> performAction() {
        List<String> errors=null;
          try {
              
              
              ///----------------------Object of salaryIncome to be edited---------------------/
                //Object of SalaryIncome to retrieve the actual object of Salary Income
                Salaryincome salaryIncome = new Salaryincome();
                salaryIncome.setSalIncId(employerAllowancesWrapper.getSalaryIncomeID());
                
                
                //Object of SalaryIncome Dao
                SalaryIncomeDao salaryIncomeDao = new SalaryIncomeDao(session);
                
                
                //Retrieve the actual object of SalaryIncome and update it with employer details
                salaryIncome=salaryIncomeDao.get(salaryIncome); 
                
                
                
                
              ///-----------------Object of AllowanceType to be inserted------------------------///
               
                //Object of Dao of AllowanceType
                AllowancesDao allowancesDao = new AllowancesDao(session);
                
                
                //Object of Allowance to retrieve object of Allowance
                Allowances allowance = new Allowances();
                allowance.setAllwceId(employerAllowancesWrapper.getAllowanceTypeID());
                
               
                allowance=allowancesDao.get(allowance);
                
                 
                
                
                
            
                
                
            ///----------------Object of employerAllowance to be inserted in the database-------------------///
                
                Employerallowance employerAllowance = new Employerallowance();
                employerAllowance.setAdditionalValue(employerAllowancesWrapper.getAllowanceAdditionalAmount());
                employerAllowance.setValue(employerAllowancesWrapper.getAllowanceAmount());
                employerAllowance.setAllowances(allowance);
                employerAllowance.setSalaryincome(salaryIncome);
              
                
               EmployerAllowanceDao employerAllowanceDao = new EmployerAllowanceDao(session);
                
                
                
                //save the object of employerAllowance
                EmployerAllowanceID = employerAllowanceDao.save(employerAllowance);
             /*   employerAllowance=new Employerallowance();
                employerAllowance.setEmpAllwceId(EmployerAllowanceID);
                
                //Retrieve the above object for salaryIncome Updation
                employerAllowance = employerAllowanceDao.get(employerAllowance);
          
                
                //update the salaryIncome Object
                Set employerAllowances = salaryIncome.getEmployerallowances();
                employerAllowances.add(employerAllowance);
                salaryIncome.setEmployerallowances(employerAllowances);
                salaryIncomeDao.update(salaryIncome);*/
                
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
    
}
