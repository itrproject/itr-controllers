/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.DeductionsForDisabilityWrapper;
import com.finmart.tax.dao.Deductionfordisabilitycertification;
import com.finmart.tax.dao.Deductions;
import com.finmart.tax.dao.Deductionsforpersonaldisability;
import com.finmart.tax.dao.Deductiontypes;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.DeductionTypeDao;
import com.finmart.tax.dao.action.DeductionsDao;
import com.finmart.tax.dao.action.DeductionsForDisabilityCertificationDao;
import com.finmart.tax.dao.action.DeductionsForPersonalDisabilityDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.AbstractSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveDeductionForDisability implements Action{
    private int UserID;
    private Integer DeductionTypeID=5;

    public SaveDeductionForDisability(DeductionsForDisabilityWrapper deductionsForDisabilityWrapper, Session session) {
        this.deductionsForDisabilityWrapper = deductionsForDisabilityWrapper;
        this.session = session;
    }

    DeductionsForDisabilityWrapper deductionsForDisabilityWrapper;
    Session session;
    @Override
    public List<String> performAction() {
        List<String> errors=null;
         try {
             //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = deductionsForDisabilityWrapper.getUserID();
            
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();
            ///-----------------Object of DeductionType type to be inserted------------------------///
                //Object of Dao of Deduction type
                DeductionTypeDao deductionTypeDao = new DeductionTypeDao(session);
                
                
                //Object of DeductionType to retrieve object of DeductionType
                Deductiontypes deductionTypes = new Deductiontypes();
                deductionTypes.setDeductionTypesId(DeductionTypeID);
                deductionTypes = deductionTypeDao.get(deductionTypes);
                
                
            ///---------------Object of Deductions for insertion in the income table-------------///
                

                
                //Create object of Deductions to save it to income table
                Deductions deductions = new Deductions();
                deductions.setUseritmapping(useritmapping);
                deductions.setDeductiontypes(deductionTypes);
                
                
                //Create object of Deducitons to save the Deductions object and Retrieve ID of the lastly inserted income ID
                DeductionsDao deductionsDao = new DeductionsDao(session);
               deductionsDao.save(deductions);
               
                 
                                                                                
              ///-----------------Object of DeductionForPersonalDisability to be inserted------------------------///
                //Object of Dao of DisabilityCertification
                DeductionsForDisabilityCertificationDao deductionsForDisabilityCertificationDao = new DeductionsForDisabilityCertificationDao(session);
                
                
                //Object of DisabilityCertification to retrieve object of DisabilityCertification
                Deductionfordisabilitycertification deductionForPersonalDisabilityCertification = new Deductionfordisabilitycertification();
                deductionForPersonalDisabilityCertification.setDeductionForDisabilityCertificationId(deductionsForDisabilityWrapper.getPersonalDisabilityCertificationID());
                deductionForPersonalDisabilityCertification = deductionsForDisabilityCertificationDao.get(deductionForPersonalDisabilityCertification);
               
                ///-----------------Object of DeductionForDependentDisability to be inserted------------------------///
               
                //Object of DisabilityCertification to retrieve object of DisabilityCertification
                Deductionfordisabilitycertification deductionForDependentDisabilityCertification = new Deductionfordisabilitycertification();
                deductionForDependentDisabilityCertification.setDeductionForDisabilityCertificationId(deductionsForDisabilityWrapper.getDependentDisabilityCertificationID());
                deductionForDependentDisabilityCertification = deductionsForDisabilityCertificationDao.get(deductionForDependentDisabilityCertification);
               
                
             ///----------------Object of DeductionsForDisability to be inserted in the database-----------------///
                //Object of DeductionsForDisability
                Deductionsforpersonaldisability deductionsForPersonalDisability = new Deductionsforpersonaldisability();
                deductionsForPersonalDisability.setDeductionfordisabilitycertificationByDependentDisabilityCertificationId(deductionForDependentDisabilityCertification);
                deductionsForPersonalDisability.setDeductionfordisabilitycertificationByPersonalDisabilityCertificationId(deductionForPersonalDisabilityCertification);
                deductionsForPersonalDisability.setDeductions(deductions);
                
                
                
                //Object of DeductionsForDisability Dao
                DeductionsForPersonalDisabilityDao deductionsForPersonalDisabilityDao = new DeductionsForPersonalDisabilityDao(session);
                
                
                //save the DeductionsForDisability for the user
                deductionsForPersonalDisabilityDao.save(deductionsForPersonalDisability);
             
            
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }

   
    
}
