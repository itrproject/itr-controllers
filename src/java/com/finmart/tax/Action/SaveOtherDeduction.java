/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.OtherDeductionsWrapper;
import com.finmart.tax.dao.Deductions;
import com.finmart.tax.dao.Deductiontypes;
import com.finmart.tax.dao.Deductionundersectiontype;
import com.finmart.tax.dao.Otherdeductions;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.DeductionTypeDao;
import com.finmart.tax.dao.action.DeductionsDao;
import com.finmart.tax.dao.action.DeductionsUnderSectionTypeDao;
import com.finmart.tax.dao.action.OtherDeductionsDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveOtherDeduction implements Action{
    private int UserID;
    private Integer DeductionTypeID=4;

    public SaveOtherDeduction(OtherDeductionsWrapper otherDeductionsWrapper, Session session) {
        this.otherDeductionsWrapper = otherDeductionsWrapper;
        this.session = session;
    }
    
    OtherDeductionsWrapper otherDeductionsWrapper;
    Session session;

    @Override
    public List<String> performAction() {
        List<String> errors=null;
         try {
             //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = otherDeductionsWrapper.getUserID();
             CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of DeductionType type to be inserted------------------------///
                //Object of Dao of Deduction type
                DeductionTypeDao deductionTypeDao = new DeductionTypeDao(session);
                
                
                //Object of DeductionType to retrieve object of DeductionType
                Deductiontypes deductionTypes = new Deductiontypes();
                deductionTypes.setDeductionTypesId(DeductionTypeID);
                deductionTypes = deductionTypeDao.get(deductionTypes);
                
                
            ///---------------Object of Deductions for insertion in the income table-------------///
                

                
                //Create object of Deductions to save it to income table
                Deductions deductions = new Deductions();
                deductions.setUseritmapping(useritmapping);
                deductions.setDeductiontypes(deductionTypes);
                
                
                //Create object of Deducitons to save the Deductions object and Retrieve ID of the lastly inserted income ID
                DeductionsDao deductionsDao = new DeductionsDao(session);
                deductionsDao.save(deductions);
                
              
                

           ///-----------------Object of DeductionUnderSectionType type to be inserted------------------------///
                //Object of Dao of DeductionUnderSectionType
                DeductionsUnderSectionTypeDao deductionsUnderSectionTypeDao = new DeductionsUnderSectionTypeDao(session);
                
                
                //Object of DeductionUnderSectionType to retrieve object of DeductionUnderSectionType
                Deductionundersectiontype deductionUnderSectionType = new Deductionundersectiontype();
                deductionUnderSectionType.setDeductionUnderSectionTypeId(otherDeductionsWrapper.getDeductionUnderSectionType());
                deductionUnderSectionType = deductionsUnderSectionTypeDao.get(deductionUnderSectionType);
               
                                                                                
         
             ///---------------Object of OtherDeductions for insertion in the OtherDeductions table-------------///
           
                //Object of OtherDeductions
                Otherdeductions otherDeductions = new Otherdeductions();
                otherDeductions.setAmountOfDeductions(otherDeductionsWrapper.getAmountOfDeduction());
                otherDeductions.setDeductions(deductions);
                otherDeductions.setDeductionundersectiontype(deductionUnderSectionType);
                
                
                
                //Object of OtherDeductions Dao
                OtherDeductionsDao otherDeductionsDao = new OtherDeductionsDao(session);
                
                
                //save the OtherDeductions for the user
                otherDeductionsDao.save(otherDeductions);
                
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }

    
    
}
