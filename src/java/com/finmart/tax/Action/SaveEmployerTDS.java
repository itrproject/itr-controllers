/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.EmployerTDSWrapper;
import com.finmart.tax.dao.Employertdsinfo;
import com.finmart.tax.dao.Taxespaid;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.EmployerTDSDao;
import com.finmart.tax.dao.action.TaxesPaidDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveEmployerTDS implements Action{
    private int UserID;
    private int TaxesPaidID;

    public SaveEmployerTDS(EmployerTDSWrapper employerTDSWrapper, Session session) {
        this.employerTDSWrapper = employerTDSWrapper;
        this.session = session;
    }

    EmployerTDSWrapper employerTDSWrapper;
    Session session;
    @Override
    public List<String> performAction() {
         List<String> errors=null;
          try {
              //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
             //fetch the user ID
            //fetch the user ID
            UserID = employerTDSWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of taxesPaid to be inserted------------------------///
        
        
            
                //Object of Dao of taxesPaid
                TaxesPaidDao taxesPaidDao = new TaxesPaidDao(session);
                
                
                //Object of taxesPaid to be inserted
                Taxespaid taxesPaid = new Taxespaid();
                taxesPaid.setUseritmapping(useritmapping);
                
               
                taxesPaidDao.save(taxesPaid);
                
                
               
                
         
                
            
                
             ///----------------Object of EmployerTDSInfo to be inserted in the database-----------------///
                //Object of EmployerTDSInfo
                 Employertdsinfo employerTDSInfo = new Employertdsinfo();
                         
                employerTDSInfo.setAddressOfDeductor(employerTDSWrapper.getAddressOfDeductor());
                employerTDSInfo.setNameOfDeductor(employerTDSWrapper.getNameOfDeductor());
                employerTDSInfo.setTanno(employerTDSWrapper.getTanNo());
                employerTDSInfo.setTaxDeductAmount(employerTDSWrapper.getTaxDeductAmount());
                employerTDSInfo.setTaxespaid(taxesPaid);
                
                
               //Object of Dao of EmployerTDSInfo
               EmployerTDSDao employerTDSDao = new EmployerTDSDao(session);
                
                                                                            
                
                
                //save the EmployerTDSInfo for the user
                employerTDSDao.save(employerTDSInfo);
                
                
     

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }

    
    
}
