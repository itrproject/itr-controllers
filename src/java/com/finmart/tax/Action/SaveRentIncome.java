/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.RentIncomeWrapper;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Rentincome;
import com.finmart.tax.dao.Rentincomepropertytype;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.RentIncomeDao;
import com.finmart.tax.dao.action.RentIncomePropertyTypeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveRentIncome implements Action{

    public SaveRentIncome( RentIncomeWrapper rentIncomeWrapper,Session session) {
        
        this.session = session;
        this.rentIncomeWrapper = rentIncomeWrapper;
    }
    
    int UserID;
    int IncomeTypeID=2;
    Session session;
    RentIncomeWrapper rentIncomeWrapper;
    int RentIncomePropertyTypeID;
    
    @Override
    public List<String> performAction() {
        List<String> errors=null;
          try {
              //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = rentIncomeWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             
             ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);
                
               
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
               
                
             ///----------------Object of RentIncomePropertyType to be inserted in the database-----------------///
              RentIncomePropertyTypeID=rentIncomeWrapper.getTypeOfPropertyID();
               //Object of Dao of RentIncomePropertytype
               RentIncomePropertyTypeDao rentIncomePropertyTypeDao = new RentIncomePropertyTypeDao(session);
                
                
                //Object of RentIncomePropertyType to retrieve object of RentIncomePropertyType
                Rentincomepropertytype rentIncomePropertyType = new Rentincomepropertytype();
                rentIncomePropertyType.setRentIncomePropertyTypeId(RentIncomePropertyTypeID);
                
               
                rentIncomePropertyType = rentIncomePropertyTypeDao.get(rentIncomePropertyType);
                                                                                
                
             ///----------------Object of Rent income to be inserted in the database-----------------///
                //Object of RentIncome
                Rentincome rentIncome = new Rentincome();
                rentIncome.setIncome(income);
                rentIncome.setNameOfTenant(rentIncomeWrapper.getNameOfTenant());
                rentIncome.setTenantPan(rentIncomeWrapper.getTenantPan());
                rentIncome.setActualRentRecieved(rentIncomeWrapper.getActualRentReceived());
                rentIncome.setPropertyTaxPaid(rentIncomeWrapper.getPropertyTaxPaid());
                rentIncome.setRentincomepropertytype(rentIncomePropertyType);
                
                
                //Object of RentIncome Dao
                RentIncomeDao rentIncomeDao = new RentIncomeDao(session);
         
                
                
                //save the Rent income for the user
                rentIncomeDao.save(rentIncome);
                
                
             
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    
}
    
}
