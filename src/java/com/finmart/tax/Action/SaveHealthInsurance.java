/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.HealthInsuranceWrapper;
import com.finmart.tax.dao.Deductions;
import com.finmart.tax.dao.Deductiontypes;
import com.finmart.tax.dao.Healthinsurance;
import com.finmart.tax.dao.Healthinsurancecheckup;
import com.finmart.tax.dao.Healthinsurancepremium;
import com.finmart.tax.dao.Insuranceparticularsfor;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.DeductionTypeDao;
import com.finmart.tax.dao.action.DeductionsDao;
import com.finmart.tax.dao.action.HealthInsuranceCheckupDao;
import com.finmart.tax.dao.action.HealthInsuranceDao;
import com.finmart.tax.dao.action.HealthInsurancePremiumDao;
import com.finmart.tax.dao.action.InsuranceParticularsForDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveHealthInsurance implements Action{
    private int UserID;
    private Integer DeductionTypeID=6;

    public SaveHealthInsurance(HealthInsuranceWrapper healthInsuranceWrapper, Session session) {
        this.healthInsuranceWrapper = healthInsuranceWrapper;
        this.session = session;
    }

    HealthInsuranceWrapper healthInsuranceWrapper;
    Session session;
    @Override
    public List<String> performAction() {
        List<String> errors=null;
         try {
            //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
             //fetch the user ID
            UserID = healthInsuranceWrapper.getUserID();

            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();
             
            ///-----------------Object of DeductionType type to be inserted------------------------///
                //Object of Dao of Deduction type
                DeductionTypeDao deductionTypeDao = new DeductionTypeDao(session);
                
                
                //Object of DeductionType to retrieve object of DeductionType
                Deductiontypes deductionTypes = new Deductiontypes();
                deductionTypes.setDeductionTypesId(DeductionTypeID);
                deductionTypes = deductionTypeDao.get(deductionTypes);
                
                
            ///---------------Object of Deductions for insertion in the income table-------------///
                

                
                //Create object of Deductions to save it to income table
                Deductions deductions = new Deductions();
                deductions.setUseritmapping(useritmapping);
                deductions.setDeductiontypes(deductionTypes);
                
                
                //Create object of Deducitons to save the Deductions object and Retrieve ID of the lastly inserted income ID
                DeductionsDao deductionsDao = new DeductionsDao(session);
               deductionsDao.save(deductions);
              
                                                                                
              ///-----------------Object of HealthInsurance to be inserted------------------------///
                //Object of Dao of HealthInsurance
                HealthInsuranceDao healthInsuranceDao = new HealthInsuranceDao(session);
                
                
                //Object of HealthInsurance
                Healthinsurance healthInsurance = new Healthinsurance();
                healthInsurance.setDeductions(deductions);
                
                healthInsuranceDao.save(healthInsurance);
              
                
                
              ////-------------InsuranceParticularsFor----------------------///
                InsuranceParticularsForDao insuranceParticularsForDao = new InsuranceParticularsForDao(session);
                
                Insuranceparticularsfor insuranceParticularsForSelf = new Insuranceparticularsfor();
                insuranceParticularsForSelf.setParticularsForId(1);
                insuranceParticularsForSelf = insuranceParticularsForDao.get(insuranceParticularsForSelf);
                
                
                Insuranceparticularsfor insuranceParticularsForParents = new Insuranceparticularsfor();
                insuranceParticularsForParents.setParticularsForId(2);
                insuranceParticularsForParents = insuranceParticularsForDao.get(insuranceParticularsForParents);
                
                
                
                
             ///----------------Object of HealthInsurancePremium to be inserted in the database------------///
                
                // Object of HealthInsurancePremium Dao
                HealthInsurancePremiumDao healthInsurancePremiumDao = new HealthInsurancePremiumDao(session);
                
                //Object of HealthInsurancePremium for self
                Healthinsurancepremium healthInsurancePremiumForSelf = new Healthinsurancepremium();
                healthInsurancePremiumForSelf.setHealthinsurance(healthInsurance);
                healthInsurancePremiumForSelf.setInsuranceparticularsfor(insuranceParticularsForSelf);
                healthInsurancePremiumForSelf.setPremiumAmounts(healthInsuranceWrapper.getPremiumForSelf());
                
                //save the healthInsurancePremiumForSelf object
                healthInsurancePremiumDao.save(healthInsurancePremiumForSelf);
                
                //Object of HealthInsurancePremium for parents
                Healthinsurancepremium healthInsurancePremiumForParents = new Healthinsurancepremium();
                healthInsurancePremiumForParents.setHealthinsurance(healthInsurance);
                healthInsurancePremiumForParents.setInsuranceparticularsfor(insuranceParticularsForParents);
                healthInsurancePremiumForParents.setPremiumAmounts(healthInsuranceWrapper.getPremiumForParents());
                
                
                //save the healthInsurancePremiumForParents object
                healthInsurancePremiumDao.save(healthInsurancePremiumForParents);
                
                
            ///----------------Object of HealthInsuranceCheckup to be inserted in the database------------///
                
                // Object of HealthInsurancecheckup Dao
                HealthInsuranceCheckupDao healthInsuranceCheckupDao = new HealthInsuranceCheckupDao(session);
                
                //Object of HealthInsurancecheckup for self
                Healthinsurancecheckup healthInsuranceCheckupForSelf = new Healthinsurancecheckup();
                healthInsuranceCheckupForSelf.setHealthinsurance(healthInsurance);
                healthInsuranceCheckupForSelf.setInsuranceparticularsfor(insuranceParticularsForSelf);
                healthInsuranceCheckupForSelf.setCheckUpAmounts(healthInsuranceWrapper.getCheckupForSelf());
                
                //save the healthInsurancePremiumForSelf object
                healthInsuranceCheckupDao.save(healthInsuranceCheckupForSelf);
                
                //Object of HealthInsurancecheckup for parents
                Healthinsurancecheckup healthInsuranceCheckupForParents = new Healthinsurancecheckup();
                healthInsuranceCheckupForParents.setHealthinsurance(healthInsurance);
                healthInsuranceCheckupForParents.setInsuranceparticularsfor(insuranceParticularsForParents);
                healthInsuranceCheckupForParents.setCheckUpAmounts(healthInsuranceWrapper.getCheckupForParents());
                
                //save the healthInsurancePremiumForSelf object
                healthInsuranceCheckupDao.save(healthInsuranceCheckupForParents);
               
                

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }

    
}
