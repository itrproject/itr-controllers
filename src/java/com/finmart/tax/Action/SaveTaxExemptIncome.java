/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.TaxExemptIncomeWrapper;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Taxexemptincome;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.TaxExemptIncomeDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveTaxExemptIncome implements Action {

    //stores user ID of the user
    int UserID;
    int IncomeTypeID = 1;
    Session session;
    private TaxExemptIncomeWrapper taxExemptIncomeWrapper;
    public SaveTaxExemptIncome(TaxExemptIncomeWrapper taxExemptIncomeWrapper,Session session) {
        this.taxExemptIncomeWrapper=taxExemptIncomeWrapper;
        this.session = session;
    }
    
    @Override
    public List<String> performAction() {
        List<String> errors = null;

        try {
            //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = taxExemptIncomeWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

               
        ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);        
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
                
                                                           
                
             ///----------------Object of taxexempt income to be inserted in the database-----------------///
                //Object of Tax Exempt Income
                Taxexemptincome taxexemptincome = new Taxexemptincome();
                taxexemptincome.setIncome(income);
                taxexemptincome.setTaxExemptIncomeType(taxExemptIncomeWrapper.getTaxExemptIncomeType());
                taxexemptincome.setTaxExemptIncomeValue(taxExemptIncomeWrapper.getTaxExemptIncomeValue());
                
                
                //Object of Tax Exempt Income Dao
                TaxExemptIncomeDao taxExemptIncomeDao = new TaxExemptIncomeDao(session);
                
                
                //save the tax Exempt income for the user
                taxExemptIncomeDao.save(taxexemptincome);
                
                
                
             
        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
    
    
    
    
   
            

}
