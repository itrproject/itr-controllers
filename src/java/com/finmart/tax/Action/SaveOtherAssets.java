/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.objects.Exemption;
import com.finmart.tax.beans.wrapper.OtherAssetsWrapper;
import com.finmart.tax.dao.Capitalgain;
import com.finmart.tax.dao.Capitalgaintype;
import com.finmart.tax.dao.Exemptionfromcapitalgain;
import com.finmart.tax.dao.Exemptions;
import com.finmart.tax.dao.Exemptiontype;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Otherassets;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.action.CapitalGainDao;
import com.finmart.tax.dao.action.CapitalGainTypeDao;
import com.finmart.tax.dao.action.ExemptionFromCapitalGainDao;
import com.finmart.tax.dao.action.ExemptionTypeDao;
import com.finmart.tax.dao.action.ExemptionsDao;
import com.finmart.tax.dao.action.IncomeDao;
import com.finmart.tax.dao.action.IncometypeDao;
import com.finmart.tax.dao.action.OtherAssetsDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveOtherAssets implements Action{
    private int UserID;
    private Integer IncomeTypeID=6;
    private Integer CapitalGainTypeID=3;

    public SaveOtherAssets(OtherAssetsWrapper otherAssetsWrapper, Session session) {
        this.otherAssetsWrapper = otherAssetsWrapper;
        this.session = session;
    }

    OtherAssetsWrapper otherAssetsWrapper;
    Session session;
    @Override
    public List<String> performAction() {
        List<String> errors=null;
        //  try {
        //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = otherAssetsWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of income type to be inserted------------------------///
                //Object of Dao of income type
                IncometypeDao incometypeDao = new IncometypeDao(session);
                
                
                //Object of income type to retrieve object of incomeType
                Incometype incomeType = new Incometype();
                incomeType.setIncomeTypeId(IncomeTypeID);
                
               
                incomeType = incometypeDao.get(incomeType);
                
                
            ///---------------Object of income for insertion in the income table-------------///
                

                
                //Create object of income to save it to income table
                Income income = new Income();
                income.setUseritmapping(useritmapping);
                income.setIncometype(incomeType);
                
                
                //Create object of IncomeDao to save the income object and Retrieve ID of the lastly inserted income ID
                IncomeDao incomeDao = new IncomeDao(session);
                incomeDao.save(income);
                                                         
            
              ///----------------Object of CapitalGainType to be inserted in the database-----------------///
               
               //Object of Dao of CapitalGainType
               CapitalGainTypeDao capitalGainTypeDao = new CapitalGainTypeDao(session);
                
                
                //Object of CapitalGainType to retrieve object of CapitalGainType
                Capitalgaintype capitalGainType = new Capitalgaintype();
                capitalGainType.setCapitalGainTypeId(CapitalGainTypeID);
                
               
                capitalGainType = capitalGainTypeDao.get(capitalGainType);
                
             ///----------------Object of CapitalGain to be inserted in the database-----------------///
                //Object of CapitalGain
                Capitalgain capitalGain = new Capitalgain();
                capitalGain.setCapitalGainTerm(otherAssetsWrapper.getCapitalGainTerm());
                capitalGain.setCapitalgaintype(capitalGainType);
                capitalGain.setIncome(income);
                
                
                //Object of CapitalGain Dao
                CapitalGainDao capitalGainDao = new CapitalGainDao(session);
         
                
                
                //save the CapitalGain for the user
                capitalGainDao.save(capitalGain);
                
             ///----------------Object of otherAssets to be inserted in the database-----------------///
                //Object of otherAssets
                Otherassets otherAssets = new Otherassets();
                otherAssets.setCapitalgain(capitalGain);
                otherAssets.setCostDate(otherAssetsWrapper.getCostDate());
                otherAssets.setCostOfImprovement(otherAssetsWrapper.getCostOfImprovement());
                otherAssets.setCostValue(otherAssetsWrapper.getCostValue());
                otherAssets.setDateOfImprovement(otherAssetsWrapper.getDateOfImprovement());
                otherAssets.setExpenseRelatedToSale(otherAssetsWrapper.getExpenseRelatedToSale());
                otherAssets.setSaleDate(otherAssetsWrapper.getSaleDate());
                otherAssets.setSaleValue(otherAssetsWrapper.getSaleValue());
                
                
                OtherAssetsDao otherAssetsDao = new OtherAssetsDao(session);
                otherAssetsDao.save(otherAssets);
                
                   
                
            ///----------------Objects of ExemptionFromCapitalGain to be inserted in the database-------------///  
                if(otherAssetsWrapper.getAssetsExemptions().size()>0){
                    ///--------------------Object of ExemptionTypeDao--------------------------///
                    ExemptionTypeDao exemptionTypeDao = new ExemptionTypeDao(session);
                    //Object of ExemptionType
                    Exemptiontype exemptionType;
                    
                    
                    //Object of Exemptions Dao
                    ExemptionsDao exemptionsDao = new ExemptionsDao(session);
                    //Object of Exemptions
                    Exemptions exemptions;
                    
                    
                    
                    //Object of ExemptionFromCapitalGain Dao
                    ExemptionFromCapitalGainDao exemptionFromCapitalGainDao = new ExemptionFromCapitalGainDao(session);
                    //Object of ExemptionFromCapitalGain
                    Exemptionfromcapitalgain exemptionFromCapitalGain = new Exemptionfromcapitalgain();
                    
                    
                    Exemption exemption;
                    
                    
                    //Saving all the exemptions for PlotOfLand
                    for(int i=0;i<otherAssetsWrapper.getAssetsExemptions().size();i++){
                        
                        //Object to retrieve the exemption details
                        exemption = otherAssetsWrapper.getAssetsExemptions().get(i);
                        
                        
                        //Object of exemptionType to be inserted
                        exemptionType = new Exemptiontype();
                        
                        exemptionType.setExemptionTypeId(exemption.getExemptionTypeID());                                                
                        exemptionType=exemptionTypeDao.get(exemptionType);
                        
                        //Object of Exemptions to be inserted in the database
                        exemptions = new Exemptions();
                        
                        exemptions.setExemptionAmount(exemption.getExemptionAmount());
                        exemptions.setExemptiontype(exemptionType);
                        
                        exemptionsDao.save(exemptions);
                        
                        
                        
                        //Object of ExemptionFromCapitalGain
                        exemptionFromCapitalGain = new Exemptionfromcapitalgain();
                        
                        exemptionFromCapitalGain.setExemptions(exemptions);
                        exemptionFromCapitalGain.setOtherassets(otherAssets);
                        
                        exemptionFromCapitalGainDao.save(exemptionFromCapitalGain);
                        
                    }
                    
                }
                
                
             
       // } //catch (Exception e) {
            //errors.add(e.getMessage().toString());
           // System.out.println(e.getMessage());
       // }
        return errors;
    }

   
    }
    

