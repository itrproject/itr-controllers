/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.Action;

import com.finmart.tax.beans.wrapper.VoluntaryTaxPaymentWrapper;
import com.finmart.tax.dao.Taxespaid;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import com.finmart.tax.dao.Voluntarytaxpayment;
import com.finmart.tax.dao.action.TaxesPaidDao;
import com.finmart.tax.dao.action.UserMasterDao;
import com.finmart.tax.dao.action.VoluntaryTaxPaymentDao;
import com.finmart.tax.dao.action.userITMappingDao;
import com.finmart.tax.functions.CheckUserITMappingExistence;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Administrator
 */
public class SaveVoluntaryTaxPayment implements Action{

    
    int UserID;
    VoluntaryTaxPaymentWrapper voluntaryTaxPaymentWrapper;
    Session session;
    private int TaxesPaidID;
    
    public SaveVoluntaryTaxPayment(VoluntaryTaxPaymentWrapper voluntaryTaxPaymentWrapper, Session session) {
        this.voluntaryTaxPaymentWrapper = voluntaryTaxPaymentWrapper;
        this.session = session;
    }
    @Override
    public List<String> performAction() {
        List<String> errors=null;
          try {
               //Object of UserITMappnig
             Useritmapping useritmapping = new Useritmapping();
                
             ///----------------------Check the existence of User---------------------------------------///
            //fetch the user ID
            UserID = voluntaryTaxPaymentWrapper.getUserID();
            CheckUserITMappingExistence checkUserITMappingExistence = new CheckUserITMappingExistence(UserID, session);
             useritmapping=checkUserITMappingExistence.CheckUser();

             ///-----------------Object of taxesPaid to be inserted------------------------///
        
        
            
                //Object of Dao of taxesPaid
                TaxesPaidDao taxesPaidDao = new TaxesPaidDao(session);
                
                
                //Object of taxesPaid to be inserted
                Taxespaid taxesPaid = new Taxespaid();
                taxesPaid.setUseritmapping(useritmapping);
                
               
                taxesPaidDao.save(taxesPaid);
                
               
           
                
             ///----------------Object of voluntaryTaxPayment to be inserted in the database-----------------///
                //Object of voluntaryTaxPayment
                Voluntarytaxpayment voluntaryTaxPayment = new Voluntarytaxpayment();
                voluntaryTaxPayment.setAmountOfVoluntaryTaxPayment(voluntaryTaxPaymentWrapper.getAmountOfVoluntaryTaxPaid());
                voluntaryTaxPayment.setBankName(voluntaryTaxPaymentWrapper.getBranchName());
                voluntaryTaxPayment.setBsrname(voluntaryTaxPaymentWrapper.getBSRName());
                voluntaryTaxPayment.setChallanNumber(voluntaryTaxPaymentWrapper.getChallanNumber());
                voluntaryTaxPayment.setDateOfDeposit(voluntaryTaxPaymentWrapper.getDateOfDeposit());
                voluntaryTaxPayment.setTaxespaid(taxesPaid);
                
                
               //Object of Dao of voluntaryTaxPayment
               VoluntaryTaxPaymentDao voluntaryTaxPaymentDao = new VoluntaryTaxPaymentDao(session);
                
                                                                            
                
                
                //save the Rent income for the user
                voluntaryTaxPaymentDao.save(voluntaryTaxPayment);
                
                
             

        } catch (Exception e) {
            //errors.add(e.getMessage().toString());
            System.out.println(e.getMessage());
        }
        return errors;
    }
    
    
}
