/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class HealthInsuranceWrapper {

    private int UserID;
    private int PremiumForParents;
    private int PremiumForSelf;
    private int CheckupForParents;
    private int CheckupForSelf;
    public HealthInsuranceWrapper() {
    }

    public HealthInsuranceWrapper(int UserID, int PremiumForParents, int PremiumForSelf, int CheckupForParents, int CheckupForSelf) {
        this.UserID = UserID;
        this.PremiumForParents = PremiumForParents;
        this.PremiumForSelf = PremiumForSelf;
        this.CheckupForParents = CheckupForParents;
        this.CheckupForSelf = CheckupForSelf;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getPremiumForParents() {
        return PremiumForParents;
    }

    public void setPremiumForParents(int PremiumForParents) {
        this.PremiumForParents = PremiumForParents;
    }

    public int getPremiumForSelf() {
        return PremiumForSelf;
    }

    public void setPremiumForSelf(int PremiumForSelf) {
        this.PremiumForSelf = PremiumForSelf;
    }

    public int getCheckupForParents() {
        return CheckupForParents;
    }

    public void setCheckupForParents(int CheckupForParents) {
        this.CheckupForParents = CheckupForParents;
    }

    public int getCheckupForSelf() {
        return CheckupForSelf;
    }

    public void setCheckupForSelf(int CheckupForSelf) {
        this.CheckupForSelf = CheckupForSelf;
    }
   
}
