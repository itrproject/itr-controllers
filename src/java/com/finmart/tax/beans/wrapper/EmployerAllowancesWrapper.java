/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class EmployerAllowancesWrapper {

  
   
    private int AllowanceTypeID;
    private int AllowanceAmount;
    private int AllowanceAdditionalAmount;
    private int SalaryIncomeID;
    public EmployerAllowancesWrapper() {
    }

  

    public EmployerAllowancesWrapper(int AllowanceTypeID, int AllowanceAmount, int AllowanceAdditionalAmount,int SalaryIncomeID) {
        this.AllowanceTypeID = AllowanceTypeID;
        this.AllowanceAmount = AllowanceAmount;
        this.AllowanceAdditionalAmount = AllowanceAdditionalAmount;
        this.SalaryIncomeID=SalaryIncomeID;
    }
    public int getAllowanceTypeID() {
        return AllowanceTypeID;
    }

    public void setAllowanceTypeID(int AllowanceTypeID) {
        this.AllowanceTypeID = AllowanceTypeID;
    }

    public int getAllowanceAmount() {
        return AllowanceAmount;
    }

    public void setAllowanceAmount(int AllowanceAmount) {
        this.AllowanceAmount = AllowanceAmount;
    }
      public int getAllowanceAdditionalAmount() {
        return AllowanceAdditionalAmount;
    }

    public void setAllowanceAdditionalAmount(int AllowanceAdditionalAmount) {
        this.AllowanceAdditionalAmount = AllowanceAdditionalAmount;
    }
    public int getSalaryIncomeID() {
        return SalaryIncomeID;
    }

    public void setSalaryIncomeID(int SalaryIncomeID) {
        this.SalaryIncomeID = SalaryIncomeID;
    }
  
}
