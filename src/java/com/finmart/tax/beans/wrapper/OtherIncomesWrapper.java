/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class OtherIncomesWrapper{
    
    public int getUserID() {
        return UserID;
    }

    public OtherIncomesWrapper(int UserID, int IdofOtherIncome, int AmountOfIncome) {
        this.UserID = UserID;
        this.IdofOtherIncome = IdofOtherIncome;
        this.AmountOfIncome = AmountOfIncome;
    }

   
    private int UserID;
    private int IdofOtherIncome;
    private int AmountOfIncome;
    
   

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getIdofOtherIncome() {
        return IdofOtherIncome;
    }

    public void setIdofOtherIncome(int IdofOtherIncome) {
        this.IdofOtherIncome = IdofOtherIncome;
    }

    public int getAmountOfIncome() {
        return AmountOfIncome;
    }

    public void setAmountOfIncome(int AmountOfIncome) {
        this.AmountOfIncome = AmountOfIncome;
    }
    
}
