/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class InterestEarnedWrapper {

   

    
    private int UserID;
    private int InterestIncomeTypeID;
    private int InterestIncomeValue;
    private String InterestFrom;

    
     public InterestEarnedWrapper() {
    }
     public InterestEarnedWrapper(int UserID, int InterestIncomeTypeID, int InterestIncomeValue,String InterestFrom) {
        this.UserID = UserID;
        this.InterestIncomeTypeID = InterestIncomeTypeID;
        this.InterestIncomeValue = InterestIncomeValue;
        this.InterestFrom=InterestFrom;
    }
     public void setUserID(int UserID) {
        this.UserID = UserID;
    }

   
    public void setInterestIncomeValue(int InterestIncomeValue) {
        this.InterestIncomeValue = InterestIncomeValue;
    }

    public int getUserID() {
        return UserID;
    }

  

    public int getInterestIncomeValue() {
        return InterestIncomeValue;
    }
    
     public int getInterestIncomeTypeID() {
        return InterestIncomeTypeID;
    }

    public void setInterestIncomeTypeID(int InterestIncomeTypeID) {
        this.InterestIncomeTypeID = InterestIncomeTypeID;
    }

    public String getInterestFrom() {
        return InterestFrom;
    }

    public void setInterestFrom(String InterestFrom) {
        this.InterestFrom = InterestFrom;
    }
}
