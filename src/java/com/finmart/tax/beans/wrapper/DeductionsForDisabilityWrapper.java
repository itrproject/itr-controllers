/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class DeductionsForDisabilityWrapper {

    private int UserID;
    private int PersonalDisabilityCertificationID;
    private int DependentDisabilityCertificationID;
    public DeductionsForDisabilityWrapper() {
    }

    public DeductionsForDisabilityWrapper(int UserID, int PersonalDisabilityCertificationID, int DependentDisabilityCertificationID) {
        this.UserID = UserID;
        this.PersonalDisabilityCertificationID = PersonalDisabilityCertificationID;
        this.DependentDisabilityCertificationID = DependentDisabilityCertificationID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getPersonalDisabilityCertificationID() {
        return PersonalDisabilityCertificationID;
    }

    public void setPersonalDisabilityCertificationID(int PersonalDisabilityCertificationID) {
        this.PersonalDisabilityCertificationID = PersonalDisabilityCertificationID;
    }

    public int getDependentDisabilityCertificationID() {
        return DependentDisabilityCertificationID;
    }

    public void setDependentDisabilityCertificationID(int DependentDisabilityCertificationID) {
        this.DependentDisabilityCertificationID = DependentDisabilityCertificationID;
    }
    
}
