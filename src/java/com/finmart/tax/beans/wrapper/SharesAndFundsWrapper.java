/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import com.finmart.tax.objects.Exemption;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SharesAndFundsWrapper {

    private int UserID;
    private int saleValue;
    private int ExpenseRelatedToSale;
    private Date SaleDate;
    private Date CostDate;
    private int CostValue;
    private int CostOfImprovement;
    private Date DateOfImprovement;
    List<Exemption> SharesExemptions;
    private String CapitalGainTerm;
    public SharesAndFundsWrapper() {
    }

    public SharesAndFundsWrapper(int UserID, int saleValue, int ExpenseRelatedToSale, Date SaleDate, Date CostDate, int CostValue, int CostOfImprovement, Date DateOfImprovement,  String CapitalGainTerm) {
        this.UserID = UserID;
        this.saleValue = saleValue;
        this.ExpenseRelatedToSale = ExpenseRelatedToSale;
        this.SaleDate = SaleDate;
        this.CostDate = CostDate;
        this.CostValue = CostValue;
        this.CostOfImprovement = CostOfImprovement;
        this.DateOfImprovement = DateOfImprovement;
        this.SharesExemptions = SharesExemptions;
        this.CapitalGainTerm = CapitalGainTerm;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getSaleValue() {
        return saleValue;
    }

    public void setSaleValue(int saleValue) {
        this.saleValue = saleValue;
    }

    public int getExpenseRelatedToSale() {
        return ExpenseRelatedToSale;
    }

    public void setExpenseRelatedToSale(int ExpenseRelatedToSale) {
        this.ExpenseRelatedToSale = ExpenseRelatedToSale;
    }

    public Date getSaleDate() {
        return SaleDate;
    }

    public void setSaleDate(Date SaleDate) {
        this.SaleDate = SaleDate;
    }

    public Date getCostDate() {
        return CostDate;
    }

    public void setCostDate(Date CostDate) {
        this.CostDate = CostDate;
    }

    public int getCostValue() {
        return CostValue;
    }

    public void setCostValue(int CostValue) {
        this.CostValue = CostValue;
    }

    public int getCostOfImprovement() {
        return CostOfImprovement;
    }

    public void setCostOfImprovement(int CostOfImprovement) {
        this.CostOfImprovement = CostOfImprovement;
    }

    public Date getDateOfImprovement() {
        return DateOfImprovement;
    }

    public void setDateOfImprovement(Date DateOfImprovement) {
        this.DateOfImprovement = DateOfImprovement;
    }

    public List<Exemption> getSharesExemptions() {
        return SharesExemptions;
    }

    public void setSharesExemptions(List<Exemption> SharesExemptions) {
        this.SharesExemptions = SharesExemptions;
    }

    public String getCapitalGainTerm() {
        return CapitalGainTerm;
    }

    public void setCapitalGainTerm(String CapitalGainTerm) {
        this.CapitalGainTerm = CapitalGainTerm;
    }
     
}
