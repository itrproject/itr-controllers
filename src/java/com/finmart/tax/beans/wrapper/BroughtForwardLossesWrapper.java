/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class BroughtForwardLossesWrapper {

    private int UserID;
    private Date AssessmentYear;
    private int LossFromHouseProperty;
    private int LongTermCapitalLoss;
    private int ShortTermCapitalLoss;
    private int otherSourceLoss;
    private Date DateOfFiling;
    public BroughtForwardLossesWrapper() {
    }

    public BroughtForwardLossesWrapper(int UserID, Date AssessmentYear, int LossFromHouseProperty, int LongTermCapitalLoss, int ShortTermCapitalLoss, int otherSourceLoss, Date DateOfFiling) {
        this.UserID = UserID;
        this.AssessmentYear = AssessmentYear;
        this.LossFromHouseProperty = LossFromHouseProperty;
        this.LongTermCapitalLoss = LongTermCapitalLoss;
        this.ShortTermCapitalLoss = ShortTermCapitalLoss;
        this.otherSourceLoss = otherSourceLoss;
        this.DateOfFiling = DateOfFiling;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public Date getAssessmentYear() {
        return AssessmentYear;
    }

    public void setAssessmentYear(Date AssessmentYear) {
        this.AssessmentYear = AssessmentYear;
    }

    public int getLossFromHouseProperty() {
        return LossFromHouseProperty;
    }

    public void setLossFromHouseProperty(int LossFromHouseProperty) {
        this.LossFromHouseProperty = LossFromHouseProperty;
    }

    public int getLongTermCapitalLoss() {
        return LongTermCapitalLoss;
    }

    public void setLongTermCapitalLoss(int LongTermCapitalLoss) {
        this.LongTermCapitalLoss = LongTermCapitalLoss;
    }

    public int getShortTermCapitalLoss() {
        return ShortTermCapitalLoss;
    }

    public void setShortTermCapitalLoss(int ShortTermCapitalLoss) {
        this.ShortTermCapitalLoss = ShortTermCapitalLoss;
    }

    public int getOtherSourceLoss() {
        return otherSourceLoss;
    }

    public void setOtherSourceLoss(int otherSourceLoss) {
        this.otherSourceLoss = otherSourceLoss;
    }

    public Date getDateOfFiling() {
        return DateOfFiling;
    }

    public void setDateOfFiling(Date DateOfFiling) {
        this.DateOfFiling = DateOfFiling;
    }
    
  
}
