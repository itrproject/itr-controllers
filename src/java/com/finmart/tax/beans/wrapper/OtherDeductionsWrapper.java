/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class OtherDeductionsWrapper {

     private int UserID;
    private int DeductionUnderSectionType;
    private int AmountOfDeduction;
    public OtherDeductionsWrapper() {
    }

    public OtherDeductionsWrapper(int UserID, int DeductionUnderSectionType, int AmountOfDeduction) {
        this.UserID = UserID;
        this.DeductionUnderSectionType = DeductionUnderSectionType;
        this.AmountOfDeduction = AmountOfDeduction;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getDeductionUnderSectionType() {
        return DeductionUnderSectionType;
    }

    public void setDeductionUnderSectionType(int DeductionUnderSectionType) {
        this.DeductionUnderSectionType = DeductionUnderSectionType;
    }

    public int getAmountOfDeduction() {
        return AmountOfDeduction;
    }

    public void setAmountOfDeduction(int AmountOfDeduction) {
        this.AmountOfDeduction = AmountOfDeduction;
    }
   
}
