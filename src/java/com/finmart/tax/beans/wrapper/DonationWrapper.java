/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class DonationWrapper {

     int UserID;
    int DonationTypeID;
    int DonatedAmount;
    String NameOfDonee;
    String PanOfDonee;
    String AddressOfDonee;
    int DeductionAmount;
    public DonationWrapper() {
    }

    public DonationWrapper(int UserID, int DonationTypeID, int DonatedAmount, String NameOfDonee, String PanOfDonee, String AddressOfDonee, int DeductionAmount) {
        this.UserID = UserID;
        this.DonationTypeID = DonationTypeID;
        this.DonatedAmount = DonatedAmount;
        this.NameOfDonee = NameOfDonee;
        this.PanOfDonee = PanOfDonee;
        this.AddressOfDonee = AddressOfDonee;
        this.DeductionAmount = DeductionAmount;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getDonationTypeID() {
        return DonationTypeID;
    }

    public void setDonationTypeID(int DonationTypeID) {
        this.DonationTypeID = DonationTypeID;
    }

    public int getDonatedAmount() {
        return DonatedAmount;
    }

    public void setDonatedAmount(int DonatedAmount) {
        this.DonatedAmount = DonatedAmount;
    }

    public String getNameOfDonee() {
        return NameOfDonee;
    }

    public void setNameOfDonee(String NameOfDonee) {
        this.NameOfDonee = NameOfDonee;
    }

    public String getPanOfDonee() {
        return PanOfDonee;
    }

    public void setPanOfDonee(String PanOfDonee) {
        this.PanOfDonee = PanOfDonee;
    }

    public String getAddressOfDonee() {
        return AddressOfDonee;
    }

    public void setAddressOfDonee(String AddressOfDonee) {
        this.AddressOfDonee = AddressOfDonee;
    }

    public int getDeductionAmount() {
        return DeductionAmount;
    }

    public void setDeductionAmount(int DeductionAmount) {
        this.DeductionAmount = DeductionAmount;
    }
   
}
