/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;


/**
 *
 * @author Administrator
 */
public class TaxExemptIncomeWrapper {

   
     
     private String taxExemptIncomeType;
     private int taxExemptIncomeValue;
     private int UserID;

    public TaxExemptIncomeWrapper(int UserID,String taxExemptIncomeType, int taxExemptIncomeValue) {
        this.taxExemptIncomeType = taxExemptIncomeType;
        this.taxExemptIncomeValue = taxExemptIncomeValue;
        this.UserID = UserID;
    }

   

    
     public TaxExemptIncomeWrapper(){
     }
     
     
   
      public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }
    
   

    public void setTaxExemptIncomeType(String taxExemptIncomeType) {
        this.taxExemptIncomeType = taxExemptIncomeType;
    }

    public void setTaxExemptIncomeValue(int taxExemptIncomeValue) {
        this.taxExemptIncomeValue = taxExemptIncomeValue;
    }

  
    public String getTaxExemptIncomeType() {
        return taxExemptIncomeType;
    }

    public int getTaxExemptIncomeValue() {
        return taxExemptIncomeValue;
    }
}
