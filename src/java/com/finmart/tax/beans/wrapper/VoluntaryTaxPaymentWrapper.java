/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class VoluntaryTaxPaymentWrapper {
    private int UserID;
    private String BranchName;
    private String BSRName;
    private int ChallanNumber;
    private Date DateOfDeposit;
    private int AmountOfVoluntaryTaxPaid;
   
    public VoluntaryTaxPaymentWrapper() {
    }

    public VoluntaryTaxPaymentWrapper(int UserID, String BranchName, String BSRName, int ChallanNumber, Date DateOfDeposit, int AmountOfVoluntaryTaxPaid) {
        this.UserID = UserID;
        this.BranchName = BranchName;
        this.BSRName = BSRName;
        this.ChallanNumber = ChallanNumber;
        this.DateOfDeposit = DateOfDeposit;
        this.AmountOfVoluntaryTaxPaid = AmountOfVoluntaryTaxPaid;
    }
    
     public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String BranchName) {
        this.BranchName = BranchName;
    }

    public String getBSRName() {
        return BSRName;
    }

    public void setBSRName(String BSRName) {
        this.BSRName = BSRName;
    }

    public int getChallanNumber() {
        return ChallanNumber;
    }

    public void setChallanNumber(int ChallanNumber) {
        this.ChallanNumber = ChallanNumber;
    }

    public Date getDateOfDeposit() {
        return DateOfDeposit;
    }

    public void setDateOfDeposit(Date DateOfDeposit) {
        this.DateOfDeposit = DateOfDeposit;
    }

    public int getAmountOfVoluntaryTaxPaid() {
        return AmountOfVoluntaryTaxPaid;
    }

    public void setAmountOfVoluntaryTaxPaid(int AmountOfVoluntaryTaxPaid) {
        this.AmountOfVoluntaryTaxPaid = AmountOfVoluntaryTaxPaid;
    }

}
