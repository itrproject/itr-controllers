/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import com.finmart.tax.beans.wrapper.SaveEmployerWrapper;



/**
 *
 * @author admin
 */
public class SalaryIncomeWrapper {
    
    private int UserID;
    private int EmployeeBasicSalary;
    private int EmployeeGrossSalary;
    private int EmployeeProfessionalTax;
    private int SalaryIncomeType;

   
      
    
    public SalaryIncomeWrapper() {
        
    }
 
    public SalaryIncomeWrapper(int UserID, int EmployeeBasicSalary, int EmployeeGrossSalary, int EmployeeProfessionalTax,int SalaryIncomeType) {
        this.UserID = UserID;
        this.EmployeeBasicSalary = EmployeeBasicSalary;
        this.EmployeeGrossSalary = EmployeeGrossSalary;
        this.EmployeeProfessionalTax = EmployeeProfessionalTax;
        this.SalaryIncomeType=SalaryIncomeType;
    }

  

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getEmployeeBasicSalary() {
        return EmployeeBasicSalary;
    }

    public void setEmployeeBasicSalary(int EmployeeBasicSalary) {
        this.EmployeeBasicSalary = EmployeeBasicSalary;
    }

    public int getEmployeeGrossSalary() {
        return EmployeeGrossSalary;
    }

    public void setEmployeeGrossSalary(int EmployeeGrossSalary) {
        this.EmployeeGrossSalary = EmployeeGrossSalary;
    }

    public int getEmployeeProfessionalTax() {
        return EmployeeProfessionalTax;
    }

    public void setEmployeeProfessionalTax(int EmployeeProfessionalTax) {
        this.EmployeeProfessionalTax = EmployeeProfessionalTax;
    }

    public int getSalaryIncomeType() {
        return SalaryIncomeType;
    }

    public void setSalaryIncomeType(int SalaryIncomeType) {
        this.SalaryIncomeType = SalaryIncomeType;
    }
}
