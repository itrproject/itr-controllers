/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class RentIncomeWrapper {

    

    
    
    private int UserID;
    String NameOfTenant;
    String TenantPan;
    int ActualRentReceived;
    int PropertyTaxPaid;
    int TypeOfPropertyID;
    
    
    public RentIncomeWrapper() {
    }

    public RentIncomeWrapper(int UserID, String NameOfTenant, String TenantPan, int ActualRentReceived, int PropertyTaxPaid, int TypeOfPropertyID) {
        this.UserID = UserID;
        this.NameOfTenant = NameOfTenant;
        this.TenantPan = TenantPan;
        this.ActualRentReceived = ActualRentReceived;
        this.PropertyTaxPaid = PropertyTaxPaid;
        this.TypeOfPropertyID = TypeOfPropertyID;
    }
    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getNameOfTenant() {
        return NameOfTenant;
    }

    public void setNameOfTenant(String NameOfTenant) {
        this.NameOfTenant = NameOfTenant;
    }

    public String getTenantPan() {
        return TenantPan;
    }

    public void setTenantPan(String TenantPan) {
        this.TenantPan = TenantPan;
    }

    public int getActualRentReceived() {
        return ActualRentReceived;
    }

    public void setActualRentReceived(int ActualRentReceived) {
        this.ActualRentReceived = ActualRentReceived;
    }

    public int getPropertyTaxPaid() {
        return PropertyTaxPaid;
    }

    public void setPropertyTaxPaid(int PropertyTaxPaid) {
        this.PropertyTaxPaid = PropertyTaxPaid;
    }

    public int getTypeOfPropertyID() {
        return TypeOfPropertyID;
    }

    public void setTypeOfPropertyID(int TypeOfPropertyID) {
        this.TypeOfPropertyID = TypeOfPropertyID;
    }

    
}
