/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import com.finmart.tax.objects.CoOwner;
import com.finmart.tax.objects.Exemption;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class HousePropertyWrapper {

    private int UserID;
    private String TypeOfProperty;
    private String UseOfProperty;
    private String AddressOfProperty;
    private int saleValue;
    private int ExpenseRelatedToSale;
    private Date SaleDate;
    private Date CostDate;
    private int CostValue;
    private int CostOfImprovement;
    private Date DateOfImprovement;
    private List<Exemption> HouseExemptions;
    private String CapitalGainTerm;
    private int OwnershipPercentage;
    private List<CoOwner> coOwner;
    public HousePropertyWrapper() {
    }

    public HousePropertyWrapper(int UserID, String TypeOfProperty, String UseOfProperty, String AddressOfProperty, int saleValue, int ExpenseRelatedToSale, Date SaleDate, Date CostDate, int CostValue, int CostOfImprovement, Date DateOfImprovement, String CapitalGainTerm, int OwnershipPercentage) {
        this.UserID = UserID;
        this.TypeOfProperty = TypeOfProperty;
        this.UseOfProperty = UseOfProperty;
        this.AddressOfProperty = AddressOfProperty;
        this.saleValue = saleValue;
        this.ExpenseRelatedToSale = ExpenseRelatedToSale;
        this.SaleDate = SaleDate;
        this.CostDate = CostDate;
        this.CostValue = CostValue;
        this.CostOfImprovement = CostOfImprovement;
        this.DateOfImprovement = DateOfImprovement;
        this.CapitalGainTerm = CapitalGainTerm;
        this.OwnershipPercentage = OwnershipPercentage;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getTypeOfProperty() {
        return TypeOfProperty;
    }

    public void setTypeOfProperty(String TypeOfProperty) {
        this.TypeOfProperty = TypeOfProperty;
    }

    public String getUseOfProperty() {
        return UseOfProperty;
    }

    public void setUseOfProperty(String UseOfProperty) {
        this.UseOfProperty = UseOfProperty;
    }

    public String getAddressOfProperty() {
        return AddressOfProperty;
    }

    public void setAddressOfProperty(String AddressOfProperty) {
        this.AddressOfProperty = AddressOfProperty;
    }

    public int getSaleValue() {
        return saleValue;
    }

    public void setSaleValue(int saleValue) {
        this.saleValue = saleValue;
    }

    public int getExpenseRelatedToSale() {
        return ExpenseRelatedToSale;
    }

    public void setExpenseRelatedToSale(int ExpenseRelatedToSale) {
        this.ExpenseRelatedToSale = ExpenseRelatedToSale;
    }

    public Date getSaleDate() {
        return SaleDate;
    }

    public void setSaleDate(Date SaleDate) {
        this.SaleDate = SaleDate;
    }

    public Date getCostDate() {
        return CostDate;
    }

    public void setCostDate(Date CostDate) {
        this.CostDate = CostDate;
    }

    public int getCostValue() {
        return CostValue;
    }

    public void setCostValue(int CostValue) {
        this.CostValue = CostValue;
    }

    public int getCostOfImprovement() {
        return CostOfImprovement;
    }

    public void setCostOfImprovement(int CostOfImprovement) {
        this.CostOfImprovement = CostOfImprovement;
    }

    public Date getDateOfImprovement() {
        return DateOfImprovement;
    }

    public void setDateOfImprovement(Date DateOfImprovement) {
        this.DateOfImprovement = DateOfImprovement;
    }

    public List<Exemption> getHouseExemptions() {
        return HouseExemptions;
    }

    public void setHouseExemptions(List<Exemption> HouseExemptions) {
        this.HouseExemptions = HouseExemptions;
    }

    public String getCapitalGainTerm() {
        return CapitalGainTerm;
    }

    public void setCapitalGainTerm(String CapitalGainTerm) {
        this.CapitalGainTerm = CapitalGainTerm;
    }

    public int getOwnershipPercentage() {
        return OwnershipPercentage;
    }

    public void setOwnershipPercentage(int OwnershipPercentage) {
        this.OwnershipPercentage = OwnershipPercentage;
    }

    public List<CoOwner> getCoOwner() {
        return coOwner;
    }

    public void setCoOwner(List<CoOwner> coOwner) {
        this.coOwner = coOwner;
    }
    
}
