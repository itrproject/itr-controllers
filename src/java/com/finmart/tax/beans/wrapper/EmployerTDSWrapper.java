/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class EmployerTDSWrapper {

    private int UserID;
    private int TanNo;
    private int TaxDeductAmount;
    private String NameOfDeductor;
    private String AddressOfDeductor;
    public EmployerTDSWrapper() {
    }

    public EmployerTDSWrapper(int UserID, int TanNo, int TaxDeductAmount, String NameOfDeductor, String AddressOfDeductor) {
        this.UserID = UserID;
        this.TanNo = TanNo;
        this.TaxDeductAmount = TaxDeductAmount;
        this.NameOfDeductor = NameOfDeductor;
        this.AddressOfDeductor = AddressOfDeductor;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getTanNo() {
        return TanNo;
    }

    public void setTanNo(int TanNo) {
        this.TanNo = TanNo;
    }

    public int getTaxDeductAmount() {
        return TaxDeductAmount;
    }

    public void setTaxDeductAmount(int TaxDeductAmount) {
        this.TaxDeductAmount = TaxDeductAmount;
    }

    public String getNameOfDeductor() {
        return NameOfDeductor;
    }

    public void setNameOfDeductor(String NameOfDeductor) {
        this.NameOfDeductor = NameOfDeductor;
    }

    public String getAddressOfDeductor() {
        return AddressOfDeductor;
    }

    public void setAddressOfDeductor(String AddressOfDeductor) {
        this.AddressOfDeductor = AddressOfDeductor;
    }

   
    
    
    
}
