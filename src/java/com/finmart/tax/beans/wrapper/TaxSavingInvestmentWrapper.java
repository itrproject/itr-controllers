/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author Administrator
 */
public class TaxSavingInvestmentWrapper {

    private int UserID;
    private int InvestementToolsTypesID;
    private int InvestmentAmount;
    public TaxSavingInvestmentWrapper() {
    }

    public TaxSavingInvestmentWrapper(int UserID, int InvestementToolsTypesID, int InvestmentAmount) {
        this.UserID = UserID;
        this.InvestementToolsTypesID = InvestementToolsTypesID;
        this.InvestmentAmount = InvestmentAmount;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getInvestementToolsTypesID() {
        return InvestementToolsTypesID;
    }

    public void setInvestementToolsTypesID(int InvestementToolsTypesID) {
        this.InvestementToolsTypesID = InvestementToolsTypesID;
    }

    public int getInvestmentAmount() {
        return InvestmentAmount;
    }

    public void setInvestmentAmount(int InvestmentAmount) {
        this.InvestmentAmount = InvestmentAmount;
    }
 
   
    
}
