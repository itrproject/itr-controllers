/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import com.finmart.tax.dao.Employertype;

/**
 *
 * @author admin
 */
public class SaveEmployerWrapper {

    

    
    private String EmployerName;
    private int EmployerTypeID;
    private String EmployerAddress;
    private int EmploymentPeriod;
    private int EmployerCity;
    private int SalaryIncomeID;
    private String EmployerTANNo;

   
    
    public SaveEmployerWrapper() {
    }

    public SaveEmployerWrapper(String EmployerName, int EmployerTypeID, String EmployerAddress, int EmploymentPeriod, int EmployerCity,int SalaryIncomeID,String EmployerTANNo) {
        this.EmployerName = EmployerName;
        this.EmployerTypeID = EmployerTypeID;
        this.EmployerAddress = EmployerAddress;
        this.EmploymentPeriod = EmploymentPeriod;
        this.EmployerCity = EmployerCity;
        this.SalaryIncomeID=SalaryIncomeID;
        this.EmployerTANNo=EmployerTANNo;
    }

    
   
    public String getEmployerTANNo() {
        return EmployerTANNo;
    }

    public void setEmployerTANNo(String EmployerTANNo) {
        this.EmployerTANNo = EmployerTANNo;
    }
    
   

    public String getEmployerName() {
        return EmployerName;
    }

    public void setEmployerName(String EmployerName) {
        this.EmployerName = EmployerName;
    }

    public int getEmployerTypeID() {
        return EmployerTypeID;
    }

    public void setEmployerTypeID(int EmployerTypeID) {
        this.EmployerTypeID = EmployerTypeID;
    }

    public String getEmployerAddress() {
        return EmployerAddress;
    }

    public void setEmployerAddress(String EmployerAddress) {
        this.EmployerAddress = EmployerAddress;
    }

    public int getEmploymentPeriod() {
        return EmploymentPeriod;
    }

    public void setEmploymentPeriod(int EmploymentPeriod) {
        this.EmploymentPeriod = EmploymentPeriod;
    }

    public int getEmployerCity() {
        return EmployerCity;
    }

    public void setEmployerCity(int EmployerCity) {
        this.EmployerCity = EmployerCity;
    }
     public int getSalaryIncomeID() {
        return SalaryIncomeID;
    }

    public void setSalaryIncomeID(int SalaryIncomeID) {
        this.SalaryIncomeID = SalaryIncomeID;
    }
}
