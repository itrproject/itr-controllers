/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.tests;

import com.finmart.hibernateUtil.NewHibernateUtil;
import com.finmart.tax.objects.CoOwner;
import com.finmart.tax.objects.Exemption;
import com.finmart.tax.Action.SaveBroughtForwardLosses;
import com.finmart.tax.Action.SaveDeductionForDisability;
import com.finmart.tax.Action.SaveDonation;
import com.finmart.tax.Action.SaveEmployerAllowance;
import com.finmart.tax.Action.SaveEmployerInfo;
import com.finmart.tax.Action.SaveEmployerTDS;
import com.finmart.tax.Action.SaveHealthInsurance;
import com.finmart.tax.Action.SaveHouseProperty;
import com.finmart.tax.Action.SaveInterestEarned;
import com.finmart.tax.Action.SaveOtherAssets;
import com.finmart.tax.Action.SaveOtherDeduction;
import com.finmart.tax.Action.SaveOtherIncome;
import com.finmart.tax.Action.SavePlotOFLand;
import com.finmart.tax.Action.SaveRentIncome;
import com.finmart.tax.Action.SaveSalaryIncome;
import com.finmart.tax.Action.SaveSharesAndFunds;
import com.finmart.tax.Action.SaveTaxExemptIncome;
import com.finmart.tax.Action.SaveTaxSavingInvestment;
import com.finmart.tax.Action.SaveVoluntaryTaxPayment;
import com.finmart.tax.beans.wrapper.BroughtForwardLossesWrapper;
import com.finmart.tax.beans.wrapper.DeductionsForDisabilityWrapper;
import com.finmart.tax.beans.wrapper.DonationWrapper;
import com.finmart.tax.beans.wrapper.EmployerAllowancesWrapper;
import com.finmart.tax.beans.wrapper.EmployerTDSWrapper;
import com.finmart.tax.beans.wrapper.HealthInsuranceWrapper;
import com.finmart.tax.beans.wrapper.HousePropertyWrapper;
import com.finmart.tax.beans.wrapper.InterestEarnedWrapper;
import com.finmart.tax.beans.wrapper.OtherAssetsWrapper;
import com.finmart.tax.beans.wrapper.OtherDeductionsWrapper;
import com.finmart.tax.beans.wrapper.OtherIncomesWrapper;
import com.finmart.tax.beans.wrapper.PlotOfLandWrapper;
import com.finmart.tax.beans.wrapper.RentIncomeWrapper;
import com.finmart.tax.beans.wrapper.SalaryIncomeWrapper;
import com.finmart.tax.beans.wrapper.SaveEmployerWrapper;
import com.finmart.tax.beans.wrapper.SharesAndFundsWrapper;
import com.finmart.tax.beans.wrapper.TaxExemptIncomeWrapper;
import com.finmart.tax.beans.wrapper.TaxSavingInvestmentWrapper;
import com.finmart.tax.beans.wrapper.VoluntaryTaxPaymentWrapper;
import com.finmart.tax.dao.CoOwners;
import com.finmart.tax.dao.Incometype;
import com.finmart.tax.dao.Salaryincome;
import com.finmart.tax.dao.action.IncometypeDao;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author admin
 */
public class Tester1 {
    public static void main(String[] args) {
        
        
        
        //open session
         Session session=beginTransation();
         
         
         
         //try{
         
        //Tax Exempt Income test
        /*TaxExemptIncomeWrapper taxExemptIncomeWrapper = new TaxExemptIncomeWrapper(99,"Mutual Funds",110);       
        SaveTaxExemptIncome saveTaxExemptIncome = new SaveTaxExemptIncome(taxExemptIncomeWrapper,session);
        saveTaxExemptIncome.performAction();*/
        
        //InterestEarned Test
       /* InterestEarnedWrapper interestEarnedWrapper = new InterestEarnedWrapper(89,1,500,"ICICI");
        SaveInterestEarned saveInterestEarned = new SaveInterestEarned(interestEarnedWrapper,session);
        saveInterestEarned.performAction();*/
        
        
        //Rent Income Earned Test
        /*RentIncomeWrapper rentIncomeWrapper = new RentIncomeWrapper(94,"Anish","ABPBV54321M",500,200,2);
        SaveRentIncome saveRentIncome = new SaveRentIncome(rentIncomeWrapper, session);
        saveRentIncome.performAction();*/
         
         
         //Salary Income Test
         SalaryIncomeWrapper salaryIncomeWrapper = new SalaryIncomeWrapper( 109, 100000, 120000, 2500, 1);
         SaveSalaryIncome savaSalaryIncome = new SaveSalaryIncome(salaryIncomeWrapper, session);
         savaSalaryIncome.performAction();
         
         
         //other income Test
         /*OtherIncomesWrapper otherIncomesWrapper = new OtherIncomesWrapper(92,2,1500);
         SaveOtherIncome saveOtherIncome = new SaveOtherIncome(otherIncomesWrapper, session);
         saveOtherIncome.performAction();*/
        
        //commit transaction
             
             
             
        //Employer info test
           /*  SaveEmployerWrapper saveEmployerWrapper = new SaveEmployerWrapper("Anish",2,"surat",5,1,1,"APBPV12345M");
             SaveEmployerInfo saveEmployerInfo = new SaveEmployerInfo(saveEmployerWrapper, session);
             saveEmployerInfo.performAction();*/
             
             
             
             
             //Employer Allowance test
           /*  EmployerAllowancesWrapper employerAllowancesWrapper = new EmployerAllowancesWrapper(2,500,50,1);
             SaveEmployerAllowance saveEmployerAllowance = new SaveEmployerAllowance(employerAllowancesWrapper, session);
             saveEmployerAllowance.performAction();*/
                   
                   
              //Volunatary tax Payment test
            /* Date date= new Date(2015,2,3);
             VoluntaryTaxPaymentWrapper voluntaryTaxPaymentWrapper = new VoluntaryTaxPaymentWrapper(101,"SBI","TS",123,date,100);
             SaveVoluntaryTaxPayment saveVoluntaryTaxPayment = new SaveVoluntaryTaxPayment(voluntaryTaxPaymentWrapper, session);
             saveVoluntaryTaxPayment.performAction();*/
             
             
             
             
             //Save Donation test
             /*DonationWrapper donationWrapper = new DonationWrapper(83,1,5000,"Anish", "APBPV5234M","Surat",200);
             SaveDonation saveDonation = new SaveDonation(donationWrapper, session);
             saveDonation.performAction();*/
             
             
             
             //Save TaxSavingInvestmentDao
             /*TaxSavingInvestmentWrapper taxSavingInvestmentWrapper = new TaxSavingInvestmentWrapper(100,1,500000);
             SaveTaxSavingInvestment saveTaxSavingInvestment = new SaveTaxSavingInvestment(taxSavingInvestmentWrapper, session);
             saveTaxSavingInvestment.performAction();*/
             
             
             
             //Save Brought Forward Losses
             /*Date date= new Date(2015,2,3);
             BroughtForwardLossesWrapper broughtForwardLossesWrapper = new BroughtForwardLossesWrapper(81,date,50000,10000, 500,250,date);
             SaveBroughtForwardLosses saveBroughtForwardLosses = new SaveBroughtForwardLosses(broughtForwardLossesWrapper, session);
             saveBroughtForwardLosses.performAction();*/
             
             
             
             //Save Other Deductions test
             /*OtherDeductionsWrapper otherDeductionsWrapper = new OtherDeductionsWrapper(91,1,5000);
             SaveOtherDeduction saveOtherDeduction = new SaveOtherDeduction(otherDeductionsWrapper, session);
             saveOtherDeduction.performAction();*/
             
             
             //Save Deduction for Disability test
             /*DeductionsForDisabilityWrapper deductionsForDisabilityWrapper = new DeductionsForDisabilityWrapper(82,1,3 );
             SaveDeductionForDisability saveDeductionForDisabiltiy = new SaveDeductionForDisability(deductionsForDisabilityWrapper, session);
             saveDeductionForDisabiltiy.performAction();*/
             
             //Save Health Insurance test
             /*HealthInsuranceWrapper healthInsuranceWrapper = new HealthInsuranceWrapper(86,6000,2000,5000,200);
             SaveHealthInsurance saveHealthInsurance = new SaveHealthInsurance(healthInsuranceWrapper, session);
             saveHealthInsurance.performAction();*/
             
             //Save Employer TDS Info
             /*EmployerTDSWrapper employerTDSWrapper = new EmployerTDSWrapper(84,4578,50,"anish","surat");
             SaveEmployerTDS saveEmployerTDS = new SaveEmployerTDS(employerTDSWrapper, session);
             saveEmployerTDS.performAction();*/
             
             
             //SavePlotOfLand test
           /*  List<Exemption> exemption = new ArrayList<Exemption>();
             Exemption ex = new Exemption(1,200);
             exemption.add(ex);
             ex = new Exemption(1,200);
             exemption.add(ex);
             
             List<CoOwner> coOwner = new ArrayList<CoOwner>();
             CoOwner coOwnerObj = new CoOwner("anish","ABC",10);
             coOwner.add(coOwnerObj);
             coOwnerObj = new CoOwner("Jay","DEF",10);
             coOwner.add(coOwnerObj);
             Date date= new Date(2015,2,3);
             PlotOfLandWrapper plotOfLandWrapper = new PlotOfLandWrapper(93,20000,2000,date,date,5000,5000,date, "Long Term",80);
             plotOfLandWrapper.setPlotExemptions(exemption);
             plotOfLandWrapper.setCoOwner(coOwner);
             SavePlotOFLand savePlotOFLand = new SavePlotOFLand(plotOfLandWrapper, session);
             savePlotOFLand.performAction();
         */
         
         //Save HouseProperty test
             /*List<Exemption> exemption = new ArrayList<Exemption>();
             Exemption ex = new Exemption(1,200);
             exemption.add(ex);
             ex = new Exemption(1,200);
             exemption.add(ex);
             
             List<CoOwner> coOwner = new ArrayList<CoOwner>();
             CoOwner coOwnerObj = new CoOwner("anish","ABC",10);
             coOwner.add(coOwnerObj);
             coOwnerObj = new CoOwner("Jay","DEF",10);
             coOwner.add(coOwnerObj);
             Date date= new Date(2015,2,3);
             HousePropertyWrapper housePropertyWrapper = new HousePropertyWrapper(88,"House","Staying","Surat",20000,2000,date,date,5000,5000,date, "Long Term",80);
             housePropertyWrapper.setHouseExemptions(exemption);
             housePropertyWrapper.setCoOwner(coOwner);
             SaveHouseProperty saveHouseProperty = new SaveHouseProperty(housePropertyWrapper, session);
             saveHouseProperty.performAction();*/
         
         
         //SaveSharesAndFunds test
            /* List<Exemption> exemption = new ArrayList<Exemption>();
             Exemption ex = new Exemption(1,200);
             exemption.add(ex);
             ex = new Exemption(1,200);
             exemption.add(ex);
             Date date= new Date(2015,2,3);
             SharesAndFundsWrapper sharesAndFundsWrapper = new SharesAndFundsWrapper(97,20000,2000,date,date,5000,5000,date, "Long Term");
             sharesAndFundsWrapper.setSharesExemptions(exemption);
             SaveSharesAndFunds saveSharesAndFunds = new SaveSharesAndFunds(sharesAndFundsWrapper, session);
             saveSharesAndFunds.performAction();*/
         
         
          //SaveOtherAssets test
             /*List<Exemption> exemption = new ArrayList<Exemption>();
             Exemption ex = new Exemption(1,200);
             exemption.add(ex);
             ex = new Exemption(1,200);
             exemption.add(ex);
             Date date= new Date(2015,2,3);
             OtherAssetsWrapper otherAssetsWrapper = new OtherAssetsWrapper(90,20000,2000,date,date,5000,5000,date, "Long Term");
             otherAssetsWrapper.setAssetsExemptions(exemption);
             SaveOtherAssets saveOtherAssets = new SaveOtherAssets(otherAssetsWrapper, session);
             saveOtherAssets.performAction();*/
             
            //  }catch(Exception e)
         {
            // System.out.println(e.getMessage());
             //System.exit(0);
         }
        commit(session);
    }
   
    private static Session beginTransation() {
        Session session;
        session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        return session;
    }

    private static void commit(Session session) {
        session.getTransaction().commit();
       // session.flush();
        NewHibernateUtil.shutdown();
    }
 
}
