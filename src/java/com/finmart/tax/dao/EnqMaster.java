package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * EnqMaster generated by hbm2java
 */
public class EnqMaster  implements java.io.Serializable {


     private Integer enqId;
     private AgentMaster agentMaster;
     private UserMaster userMaster;
     private AssociateloginMaster associateloginMaster;
     private ServiceMaster serviceMaster;
     private StatusMaster statusMaster;
     private CityoldMaster cityoldMaster;
     private String fromName;
     private String fromLname;
     private String fromEmail;
     private String fromMobile;
     private Date openDate;
     private Date reopenDate;
     private Date closeDate;
     private String comments;
     private String closedBy;
     private int amtPayable;
     private int govtFees;
     private int courierCharges;
     private int payStatus;
     private int pincode;
     private Set invoiceMasters = new HashSet(0);
     private Set useritmappings = new HashSet(0);
     private Set docMasters = new HashSet(0);
     private Set paymentMasters = new HashSet(0);
     private Set docuploadMasters = new HashSet(0);
     private Set restrancTables = new HashSet(0);
     private Set reqtrancTables = new HashSet(0);

    public EnqMaster() {
    }

	
    public EnqMaster(AgentMaster agentMaster, UserMaster userMaster, AssociateloginMaster associateloginMaster, ServiceMaster serviceMaster, StatusMaster statusMaster, CityoldMaster cityoldMaster, String fromName, String fromLname, String fromEmail, String fromMobile, Date openDate, Date reopenDate, Date closeDate, String comments, String closedBy, int amtPayable, int govtFees, int courierCharges, int payStatus, int pincode) {
        this.agentMaster = agentMaster;
        this.userMaster = userMaster;
        this.associateloginMaster = associateloginMaster;
        this.serviceMaster = serviceMaster;
        this.statusMaster = statusMaster;
        this.cityoldMaster = cityoldMaster;
        this.fromName = fromName;
        this.fromLname = fromLname;
        this.fromEmail = fromEmail;
        this.fromMobile = fromMobile;
        this.openDate = openDate;
        this.reopenDate = reopenDate;
        this.closeDate = closeDate;
        this.comments = comments;
        this.closedBy = closedBy;
        this.amtPayable = amtPayable;
        this.govtFees = govtFees;
        this.courierCharges = courierCharges;
        this.payStatus = payStatus;
        this.pincode = pincode;
    }
    public EnqMaster(AgentMaster agentMaster, UserMaster userMaster, AssociateloginMaster associateloginMaster, ServiceMaster serviceMaster, StatusMaster statusMaster, CityoldMaster cityoldMaster, String fromName, String fromLname, String fromEmail, String fromMobile, Date openDate, Date reopenDate, Date closeDate, String comments, String closedBy, int amtPayable, int govtFees, int courierCharges, int payStatus, int pincode, Set invoiceMasters, Set useritmappings, Set docMasters, Set paymentMasters, Set docuploadMasters, Set restrancTables, Set reqtrancTables) {
       this.agentMaster = agentMaster;
       this.userMaster = userMaster;
       this.associateloginMaster = associateloginMaster;
       this.serviceMaster = serviceMaster;
       this.statusMaster = statusMaster;
       this.cityoldMaster = cityoldMaster;
       this.fromName = fromName;
       this.fromLname = fromLname;
       this.fromEmail = fromEmail;
       this.fromMobile = fromMobile;
       this.openDate = openDate;
       this.reopenDate = reopenDate;
       this.closeDate = closeDate;
       this.comments = comments;
       this.closedBy = closedBy;
       this.amtPayable = amtPayable;
       this.govtFees = govtFees;
       this.courierCharges = courierCharges;
       this.payStatus = payStatus;
       this.pincode = pincode;
       this.invoiceMasters = invoiceMasters;
       this.useritmappings = useritmappings;
       this.docMasters = docMasters;
       this.paymentMasters = paymentMasters;
       this.docuploadMasters = docuploadMasters;
       this.restrancTables = restrancTables;
       this.reqtrancTables = reqtrancTables;
    }
   
    public Integer getEnqId() {
        return this.enqId;
    }
    
    public void setEnqId(Integer enqId) {
        this.enqId = enqId;
    }
    public AgentMaster getAgentMaster() {
        return this.agentMaster;
    }
    
    public void setAgentMaster(AgentMaster agentMaster) {
        this.agentMaster = agentMaster;
    }
    public UserMaster getUserMaster() {
        return this.userMaster;
    }
    
    public void setUserMaster(UserMaster userMaster) {
        this.userMaster = userMaster;
    }
    public AssociateloginMaster getAssociateloginMaster() {
        return this.associateloginMaster;
    }
    
    public void setAssociateloginMaster(AssociateloginMaster associateloginMaster) {
        this.associateloginMaster = associateloginMaster;
    }
    public ServiceMaster getServiceMaster() {
        return this.serviceMaster;
    }
    
    public void setServiceMaster(ServiceMaster serviceMaster) {
        this.serviceMaster = serviceMaster;
    }
    public StatusMaster getStatusMaster() {
        return this.statusMaster;
    }
    
    public void setStatusMaster(StatusMaster statusMaster) {
        this.statusMaster = statusMaster;
    }
    public CityoldMaster getCityoldMaster() {
        return this.cityoldMaster;
    }
    
    public void setCityoldMaster(CityoldMaster cityoldMaster) {
        this.cityoldMaster = cityoldMaster;
    }
    public String getFromName() {
        return this.fromName;
    }
    
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
    public String getFromLname() {
        return this.fromLname;
    }
    
    public void setFromLname(String fromLname) {
        this.fromLname = fromLname;
    }
    public String getFromEmail() {
        return this.fromEmail;
    }
    
    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }
    public String getFromMobile() {
        return this.fromMobile;
    }
    
    public void setFromMobile(String fromMobile) {
        this.fromMobile = fromMobile;
    }
    public Date getOpenDate() {
        return this.openDate;
    }
    
    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }
    public Date getReopenDate() {
        return this.reopenDate;
    }
    
    public void setReopenDate(Date reopenDate) {
        this.reopenDate = reopenDate;
    }
    public Date getCloseDate() {
        return this.closeDate;
    }
    
    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }
    public String getComments() {
        return this.comments;
    }
    
    public void setComments(String comments) {
        this.comments = comments;
    }
    public String getClosedBy() {
        return this.closedBy;
    }
    
    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }
    public int getAmtPayable() {
        return this.amtPayable;
    }
    
    public void setAmtPayable(int amtPayable) {
        this.amtPayable = amtPayable;
    }
    public int getGovtFees() {
        return this.govtFees;
    }
    
    public void setGovtFees(int govtFees) {
        this.govtFees = govtFees;
    }
    public int getCourierCharges() {
        return this.courierCharges;
    }
    
    public void setCourierCharges(int courierCharges) {
        this.courierCharges = courierCharges;
    }
    public int getPayStatus() {
        return this.payStatus;
    }
    
    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }
    public int getPincode() {
        return this.pincode;
    }
    
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
    public Set getInvoiceMasters() {
        return this.invoiceMasters;
    }
    
    public void setInvoiceMasters(Set invoiceMasters) {
        this.invoiceMasters = invoiceMasters;
    }
    public Set getUseritmappings() {
        return this.useritmappings;
    }
    
    public void setUseritmappings(Set useritmappings) {
        this.useritmappings = useritmappings;
    }
    public Set getDocMasters() {
        return this.docMasters;
    }
    
    public void setDocMasters(Set docMasters) {
        this.docMasters = docMasters;
    }
    public Set getPaymentMasters() {
        return this.paymentMasters;
    }
    
    public void setPaymentMasters(Set paymentMasters) {
        this.paymentMasters = paymentMasters;
    }
    public Set getDocuploadMasters() {
        return this.docuploadMasters;
    }
    
    public void setDocuploadMasters(Set docuploadMasters) {
        this.docuploadMasters = docuploadMasters;
    }
    public Set getRestrancTables() {
        return this.restrancTables;
    }
    
    public void setRestrancTables(Set restrancTables) {
        this.restrancTables = restrancTables;
    }
    public Set getReqtrancTables() {
        return this.reqtrancTables;
    }
    
    public void setReqtrancTables(Set reqtrancTables) {
        this.reqtrancTables = reqtrancTables;
    }




}


