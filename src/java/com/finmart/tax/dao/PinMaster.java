package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0



/**
 * PinMaster generated by hbm2java
 */
public class PinMaster  implements java.io.Serializable {


     private Integer pinId;
     private CityoldMaster cityoldMaster;
     private int pincode;

    public PinMaster() {
    }

    public PinMaster(CityoldMaster cityoldMaster, int pincode) {
       this.cityoldMaster = cityoldMaster;
       this.pincode = pincode;
    }
   
    public Integer getPinId() {
        return this.pinId;
    }
    
    public void setPinId(Integer pinId) {
        this.pinId = pinId;
    }
    public CityoldMaster getCityoldMaster() {
        return this.cityoldMaster;
    }
    
    public void setCityoldMaster(CityoldMaster cityoldMaster) {
        this.cityoldMaster = cityoldMaster;
    }
    public int getPincode() {
        return this.pincode;
    }
    
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }




}


