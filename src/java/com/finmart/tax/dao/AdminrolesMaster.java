package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0



/**
 * AdminrolesMaster generated by hbm2java
 */
public class AdminrolesMaster  implements java.io.Serializable {


     private Integer adminRoleId;
     private ServiceMaster serviceMaster;
     private AdminloginMaster adminloginMaster;

    public AdminrolesMaster() {
    }

    public AdminrolesMaster(ServiceMaster serviceMaster, AdminloginMaster adminloginMaster) {
       this.serviceMaster = serviceMaster;
       this.adminloginMaster = adminloginMaster;
    }
   
    public Integer getAdminRoleId() {
        return this.adminRoleId;
    }
    
    public void setAdminRoleId(Integer adminRoleId) {
        this.adminRoleId = adminRoleId;
    }
    public ServiceMaster getServiceMaster() {
        return this.serviceMaster;
    }
    
    public void setServiceMaster(ServiceMaster serviceMaster) {
        this.serviceMaster = serviceMaster;
    }
    public AdminloginMaster getAdminloginMaster() {
        return this.adminloginMaster;
    }
    
    public void setAdminloginMaster(AdminloginMaster adminloginMaster) {
        this.adminloginMaster = adminloginMaster;
    }




}


