/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Rentincomepropertytype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class RentIncomePropertyTypeDao implements DaoAction<Rentincomepropertytype>{

    public RentIncomePropertyTypeDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Rentincomepropertytype e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Rentincomepropertytype e) {
          session.update(e);
    }

    @Override
    public void delete(Rentincomepropertytype e) {
          session.delete(e);
    }

    @Override
    public Rentincomepropertytype get(Rentincomepropertytype e) {
        Rentincomepropertytype e1 = (Rentincomepropertytype)session.get(Rentincomepropertytype.class,e.getRentIncomePropertyTypeId());
      
        return e1;
    }

    @Override
    public List<Rentincomepropertytype> getAll() {
         List<Rentincomepropertytype> l = session.createCriteria(Rentincomepropertytype.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
