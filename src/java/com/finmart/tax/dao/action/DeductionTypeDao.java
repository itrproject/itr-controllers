/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Deductiontypes;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DeductionTypeDao implements DaoAction<Deductiontypes>{

    public DeductionTypeDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Deductiontypes e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Deductiontypes e) {
          session.update(e);
    }

    @Override
    public void delete(Deductiontypes e) {
        session.delete(e);
    }

    @Override
    public Deductiontypes get(Deductiontypes e) {
        Deductiontypes e1 = (Deductiontypes)session.get(Deductiontypes.class,e.getDeductionTypesId());
      
        return e1;
    }

    @Override
    public List<Deductiontypes> getAll() {
         List<Deductiontypes> l = session.createCriteria(Deductiontypes.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
