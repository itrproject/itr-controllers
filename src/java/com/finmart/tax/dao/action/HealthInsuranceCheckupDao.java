/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Healthinsurancecheckup;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class HealthInsuranceCheckupDao implements DaoAction<Healthinsurancecheckup>{

    public HealthInsuranceCheckupDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Healthinsurancecheckup e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Healthinsurancecheckup e) {
        session.update(e);
    }

    @Override
    public void delete(Healthinsurancecheckup e) {
        session.delete(e);
    }

    @Override
    public Healthinsurancecheckup get(Healthinsurancecheckup e) {
         Healthinsurancecheckup e1 = (Healthinsurancecheckup)session.get(Healthinsurancecheckup.class,e.getHealthInsuranceCheckUpId());
      
        return e1;
    }

    @Override
    public List<Healthinsurancecheckup> getAll() {
        List<Healthinsurancecheckup> l = session.createCriteria(Healthinsurancecheckup.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
