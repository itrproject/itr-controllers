/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Taxespaid;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class TaxesPaidDao implements DaoAction<Taxespaid>{

    public TaxesPaidDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Taxespaid e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Taxespaid e) {
          session.update(e);
    }

    @Override
    public void delete(Taxespaid e) {
          session.delete(e);
    }

    @Override
    public Taxespaid get(Taxespaid e) {
         Taxespaid e1 = (Taxespaid)session.get(Taxespaid.class,e.getTaxesPaidId());
      
        return e1;
    }

    @Override
    public List<Taxespaid> getAll() {
        List<Taxespaid> l = session.createCriteria(Taxespaid.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
