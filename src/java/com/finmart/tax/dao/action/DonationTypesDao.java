/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Donationtypes;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DonationTypesDao implements DaoAction<Donationtypes>{

    public DonationTypesDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Donationtypes e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Donationtypes e) {
         session.update(e);
    }

    @Override
    public void delete(Donationtypes e) {
          session.delete(e);
    }

    @Override
    public Donationtypes get(Donationtypes e) {
        Donationtypes e1 = (Donationtypes)session.get(Donationtypes.class,e.getDonationTypeId());
      
        return e1;
    }

    @Override
    public List<Donationtypes> getAll() {
        List<Donationtypes> l = session.createCriteria(Donationtypes.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
