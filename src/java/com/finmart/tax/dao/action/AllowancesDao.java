/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Allowances;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class AllowancesDao implements DaoAction<Allowances>{

    public AllowancesDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Allowances e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Allowances e) {
          session.update(e);
    }

    @Override
    public void delete(Allowances e) {
         session.delete(e);
    }

    @Override
    public Allowances get(Allowances e) {
         Allowances e1 = (Allowances)session.get(Allowances.class,e.getAllwceId());
      
        return e1;
    }

    @Override
    public List<Allowances> getAll() {
         List<Allowances> l = session.createCriteria(Allowances.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
