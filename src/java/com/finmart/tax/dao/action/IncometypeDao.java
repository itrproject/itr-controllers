/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.hibernateUtil.NewHibernateUtil;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Incometype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class IncometypeDao implements DaoAction<Incometype>{

    public IncometypeDao(Session session) {
        this.session = session;
    }
    Session session;
    @Override
    public int save(Incometype e) {
       
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Incometype e) {
      
        //Taxexemptincome e1 = (Taxexemptincome)session.get(TaxExemptIncomeDao.class,updateTo.getTaxExemptIncomeId());
        session.update(e);
      
    }

    @Override
    public void delete(Incometype e) {
     
        session.delete(e);
      
    }

    @Override
    public Incometype get(Incometype e) {
     
        Incometype e1 = (Incometype)session.get(Incometype.class,e.getIncomeTypeId());
      
        return e1;
    }

    @Override
    public List<Incometype> getAll() {
      
        List<Incometype> l = session.createCriteria(Incometype.class).addOrder(Order.asc("incomeTypeId")).list();
     
        return l;
    }

   
    
}
