/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Insuranceparticularsfor;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class InsuranceParticularsForDao implements DaoAction<Insuranceparticularsfor>{

    public InsuranceParticularsForDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Insuranceparticularsfor e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Insuranceparticularsfor e) {
        session.update(e);
    }

    @Override
    public void delete(Insuranceparticularsfor e) {
         session.delete(e);
    }

    @Override
    public Insuranceparticularsfor get(Insuranceparticularsfor e) {
        Insuranceparticularsfor e1 = (Insuranceparticularsfor)session.get(Insuranceparticularsfor.class,e.getParticularsForId());
      
        return e1;
    }

    @Override
    public List<Insuranceparticularsfor> getAll() {
        List<Insuranceparticularsfor> l = session.createCriteria(Insuranceparticularsfor.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
