/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Employertype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author admin
 */
public class EmployerTypeDao implements DaoAction<Employertype>{
    
    Session session;

    public EmployerTypeDao(Session session) {
        this.session = session;
    }
    
    @Override
    public int save(Employertype e) {
       session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
      
    }

     @Override
    public void update(Employertype e) {
         session.update(e);
    }


    @Override
    public void delete(Employertype e) {
         session.delete(e);
    }

    @Override
    public Employertype get(Employertype e) {
       Employertype e1 = (Employertype)session.get(Employertype.class,e.getEmptypeId());
      
        return e1;
        
    }

    @Override
    public List<Employertype> getAll() {
       List<Employertype> l = session.createCriteria(Employertype.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }

   

   
  
}
