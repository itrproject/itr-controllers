/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Deductions;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DeductionsDao implements DaoAction<Deductions>{

    public DeductionsDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Deductions e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Deductions e) {
         session.update(e);
    }

    @Override
    public void delete(Deductions e) {
         session.delete(e);
    }

    @Override
    public Deductions get(Deductions e) {
         Deductions e1 = (Deductions)session.get(Deductions.class,e.getDeductionId());
      
        return e1;
    }

    @Override
    public List<Deductions> getAll() {
         List<Deductions> l = session.createCriteria(Deductions.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
