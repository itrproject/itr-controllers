/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Exemptions;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class ExemptionsDao implements DaoAction<Exemptions>{

    public ExemptionsDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Exemptions e) {
       session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Exemptions e) {
        session.update(e);
    }

    @Override
    public void delete(Exemptions e) {
        session.delete(e);
    }

    @Override
    public Exemptions get(Exemptions e) {
         Exemptions e1 = (Exemptions)session.get(Exemptions.class,e.getExemptionId());
      
        return e1;
    }

    @Override
    public List<Exemptions> getAll() {
       List<Exemptions> l = session.createCriteria(Exemptions.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
