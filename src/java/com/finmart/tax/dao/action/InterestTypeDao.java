/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Interesttype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class InterestTypeDao implements DaoAction<Interesttype>{

    public InterestTypeDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Interesttype e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Interesttype e) {
         session.update(e);
    }

    @Override
    public void delete(Interesttype e) {
         session.delete(e);
    }

    @Override
    public Interesttype get(Interesttype e) {
         Interesttype e1 = (Interesttype)session.get(Interesttype.class,e.getInterestTypeId());
     
        return e1;
    }

    @Override
    public List<Interesttype> getAll() {
         List<Interesttype> l = session.createCriteria(Interesttype.class).addOrder(Order.asc("UId")).list();
      
        return l;
    }
    
}
