/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Investmenttoolstypes;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class InvestmentToolsTypesDao implements DaoAction<Investmenttoolstypes>{

    public InvestmentToolsTypesDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Investmenttoolstypes e) {
          session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Investmenttoolstypes e) {
         session.update(e);
    }

    @Override
    public void delete(Investmenttoolstypes e) {
        session.delete(e);
    }

    @Override
    public Investmenttoolstypes get(Investmenttoolstypes e) {
        Investmenttoolstypes e1 = (Investmenttoolstypes)session.get(Investmenttoolstypes.class,e.getInvestmentToolsTypesId());
      
        return e1;
    }

    @Override
    public List<Investmenttoolstypes> getAll() {
         List<Investmenttoolstypes> l = session.createCriteria(Investmenttoolstypes.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
