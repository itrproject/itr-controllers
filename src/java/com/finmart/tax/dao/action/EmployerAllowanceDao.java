/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Employerallowance;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class EmployerAllowanceDao implements DaoAction<Employerallowance>{

    public EmployerAllowanceDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Employerallowance e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Employerallowance e) {
       session.update(e);
    }

    @Override
    public void delete(Employerallowance e) {
       session.delete(e);
    }

    @Override
    public Employerallowance get(Employerallowance e) {
             Employerallowance e1 = (Employerallowance)session.get(Employerallowance.class,e.getEmpAllwceId());
      
        return e1;
    }

    @Override
    public List<Employerallowance> getAll() {
         List<Employerallowance> l = session.createCriteria(Employerallowance.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
