/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Voluntarytaxpayment;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class VoluntaryTaxPaymentDao implements DaoAction<Voluntarytaxpayment>{

    public VoluntaryTaxPaymentDao(Session session) {
        this.session = session;
    }

    
    Session session;
    @Override
    public int save(Voluntarytaxpayment e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Voluntarytaxpayment e) {
          session.update(e);
    }

    @Override
    public void delete(Voluntarytaxpayment e) {
          session.delete(e);
    }

    @Override
    public Voluntarytaxpayment get(Voluntarytaxpayment e) {
         Voluntarytaxpayment e1 = (Voluntarytaxpayment)session.get(Voluntarytaxpayment.class,e.getVoluntaryTaxPaymentId());
      
        return e1;
    }

    @Override
    public List<Voluntarytaxpayment> getAll() {
         List<Voluntarytaxpayment> l = session.createCriteria(Voluntarytaxpayment.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }

   
}
