/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Capitalgain;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class CapitalGainDao implements DaoAction<Capitalgain>{

    public CapitalGainDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Capitalgain e) {
          session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Capitalgain e) {
         session.update(e);
    }

    @Override
    public void delete(Capitalgain e) {
        session.delete(e);
    }

    @Override
    public Capitalgain get(Capitalgain e) {
        Capitalgain e1 = (Capitalgain)session.get(Capitalgain.class,e.getCapitalGainId());
      
        return e1;    
    }

    @Override
    public List<Capitalgain> getAll() {
        List<Capitalgain> l = session.createCriteria(Capitalgain.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
