/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Deductionsforpersonaldisability;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DeductionsForPersonalDisabilityDao implements DaoAction<Deductionsforpersonaldisability>{

    public DeductionsForPersonalDisabilityDao(Session session) {
        this.session = session;
    }

    Session session;
    
    @Override
    public int save(Deductionsforpersonaldisability e) {
           session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Deductionsforpersonaldisability e) {
       session.update(e);
    }

    @Override
    public void delete(Deductionsforpersonaldisability e) {
         session.delete(e);
    }

    @Override
    public Deductionsforpersonaldisability get(Deductionsforpersonaldisability e) {
         Deductionsforpersonaldisability e1 = (Deductionsforpersonaldisability)session.get(Deductionsforpersonaldisability.class,e.getDeductionsForPersonalDisabilityId());
      
        return e1;
    }

    @Override
    public List<Deductionsforpersonaldisability> getAll() {
         List<Deductionsforpersonaldisability> l = session.createCriteria(Deductionsforpersonaldisability.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
