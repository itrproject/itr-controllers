/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Taxsavinginvestments;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class TaxSavingInvestmentsDao implements DaoAction<Taxsavinginvestments>{

    public TaxSavingInvestmentsDao(Session session) {
        this.session = session;
    }

    Session session;
    
    @Override
    public int save(Taxsavinginvestments e) {
           session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Taxsavinginvestments e) {
        session.update(e);
    }

    @Override
    public void delete(Taxsavinginvestments e) {
         session.delete(e);
    }

    @Override
    public Taxsavinginvestments get(Taxsavinginvestments e) {
         Taxsavinginvestments e1 = (Taxsavinginvestments)session.get(Taxsavinginvestments.class,e.getTaxSavingsInvestmentsId());
      
        return e1;
    }

    @Override
    public List<Taxsavinginvestments> getAll() {
        List<Taxsavinginvestments> l = session.createCriteria(Taxsavinginvestments.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
