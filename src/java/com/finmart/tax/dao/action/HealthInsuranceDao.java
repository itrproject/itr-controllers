/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Healthinsurance;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class HealthInsuranceDao implements DaoAction<Healthinsurance>{

    public HealthInsuranceDao(Session session) {
        this.session = session;
    }

    Session session;
    
    @Override
    public int save(Healthinsurance e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Healthinsurance e) {
        session.update(e);
    }

    @Override
    public void delete(Healthinsurance e) {
         session.delete(e);
    }

    @Override
    public Healthinsurance get(Healthinsurance e) {
         Healthinsurance e1 = (Healthinsurance)session.get(Healthinsurance.class,e.getHealthInsuranceId());
      
        return e1;
    }

    @Override
    public List<Healthinsurance> getAll() {
       List<Healthinsurance> l = session.createCriteria(Healthinsurance.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
