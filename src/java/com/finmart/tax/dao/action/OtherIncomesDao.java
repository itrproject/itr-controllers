/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Otherincomes;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class OtherIncomesDao implements DaoAction<Otherincomes>{

   

    Session session;
     public OtherIncomesDao(Session session) {
        this.session = session;
    }
    @Override
    public int save(Otherincomes e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Otherincomes e) {
        session.update(e);
    }

    @Override
    public void delete(Otherincomes e) {
         session.delete(e);
    }

    @Override
    public Otherincomes get(Otherincomes e) {
         Otherincomes e1 = (Otherincomes)session.get(Otherincomes.class,e.getOtherIncomeId());
      
        return e1;
    }

    @Override
    public List<Otherincomes> getAll() {
         List<Otherincomes> l = session.createCriteria(Otherincomes.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
