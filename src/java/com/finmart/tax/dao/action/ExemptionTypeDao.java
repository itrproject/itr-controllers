/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Exemptions;
import com.finmart.tax.dao.Exemptiontype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class ExemptionTypeDao implements DaoAction<Exemptiontype>{

    public ExemptionTypeDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Exemptiontype e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Exemptiontype e) {
        session.update(e);
    }

    @Override
    public void delete(Exemptiontype e) {
       session.delete(e);
    }

    @Override
    public Exemptiontype get(Exemptiontype e) {
         Exemptiontype e1 = (Exemptiontype)session.get(Exemptiontype.class,e.getExemptionTypeId());
      
        return e1;
    }

    @Override
    public List<Exemptiontype> getAll() {
        List<Exemptiontype> l = session.createCriteria(Exemptiontype.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }

   
}
