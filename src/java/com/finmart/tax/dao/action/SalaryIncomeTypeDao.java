/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Salryincometype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class SalaryIncomeTypeDao implements DaoAction<Salryincometype>{

    
    Session session;
    public SalaryIncomeTypeDao(Session session) {
        this.session = session;
    }

    @Override
    public int save(Salryincometype e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Salryincometype e) {
         session.update(e);
    }

    @Override
    public void delete(Salryincometype e) {
        session.delete(e);
    }

    @Override
    public Salryincometype get(Salryincometype e) {
         Salryincometype e1 = (Salryincometype)session.get(Salryincometype.class,e.getSalaryIncomeTypeId());
      
        return e1;
    }

    @Override
    public List<Salryincometype> getAll() {
         List<Salryincometype> l = session.createCriteria(Salryincometype.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
