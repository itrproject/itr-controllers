/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Salaryincome;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class SalaryIncomeDao implements DaoAction<Salaryincome>{

   

    Session session;
    
     public SalaryIncomeDao(Session session) {
        this.session = session;
    }
    @Override
    public int save(Salaryincome e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Salaryincome e) {
        session.update(e);
    }

    @Override
    public void delete(Salaryincome e) {
        session.delete(e);
    }

    @Override
    public Salaryincome get(Salaryincome e) {
        Salaryincome e1 = (Salaryincome)session.get(Salaryincome.class,e.getSalIncId());
      
        return e1;
    }

    @Override
    public List<Salaryincome> getAll() {
         List<Salaryincome> l = session.createCriteria(Salaryincome.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
