/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Otherassets;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class OtherAssetsDao implements DaoAction<Otherassets>{

    public OtherAssetsDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Otherassets e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Otherassets e) {
        session.update(e);
    }

    @Override
    public void delete(Otherassets e) {
       session.delete(e);
    }

    @Override
    public Otherassets get(Otherassets e) {
        Otherassets e1 = (Otherassets)session.get(Otherassets.class,e.getAssetsId());
      
        return e1;
    }

    @Override
    public List<Otherassets> getAll() {
       List<Otherassets> l = session.createCriteria(Otherassets.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
