/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Capitalgaintype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class CapitalGainTypeDao implements DaoAction<Capitalgaintype>{

    public CapitalGainTypeDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Capitalgaintype e) {
          session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Capitalgaintype e) {
         session.update(e);
    }

    @Override
    public void delete(Capitalgaintype e) {
        session.delete(e);
    }

    @Override
    public Capitalgaintype get(Capitalgaintype e) {
         Capitalgaintype e1 = (Capitalgaintype)session.get(Capitalgaintype.class,e.getCapitalGainTypeId());
      
        return e1;
    }

    @Override
    public List<Capitalgaintype> getAll() {
       List<Capitalgaintype> l = session.createCriteria(Capitalgaintype.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
