/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.hibernateUtil.NewHibernateUtil;
import java.util.List;
import com.finmart.tax.dao.Income;
import com.finmart.tax.dao.Taxexemptincome;
import java.math.BigInteger;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
/**
 *
 * @author Administrator
 */
public class IncomeDao implements DaoAction<Income>{

    public IncomeDao(Session session) {
        this.session = session;
    }
    Session session;
    @Override
    public int save(Income e) {
        
       
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
       
    }

    @Override
    public void update(Income e) {
      
        session.update(e);
      
    }

    @Override
    public void delete(Income e) {
      
        session.delete(e);
      
    }

    @Override
    public Income get(Income e) {
       
        Income e1 = (Income)session.get(Income.class,e.getIncomeId());
      
        return e1;
    }

    @Override
    public List<Income> getAll() {
       
        List<Income> l = session.createCriteria(Income.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }

   
   
}
