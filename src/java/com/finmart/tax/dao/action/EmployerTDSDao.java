/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Employertdsinfo;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class EmployerTDSDao implements DaoAction<Employertdsinfo>{

    public EmployerTDSDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Employertdsinfo e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Employertdsinfo e) {
          session.update(e);
    }

    @Override
    public void delete(Employertdsinfo e) {
          session.delete(e);
    }

    @Override
    public Employertdsinfo get(Employertdsinfo e) {
         Employertdsinfo e1 = (Employertdsinfo)session.get(Employertdsinfo.class,e.getEmpTdsid());
      
        return e1;
    }

    @Override
    public List<Employertdsinfo> getAll() {
         List<Employertdsinfo> l = session.createCriteria(Employertdsinfo.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
