/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Houseproperty;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class HousePropertyDao implements DaoAction<Houseproperty>{

    public HousePropertyDao(Session session) {
        this.session = session;
    }

    Session session;
    public HousePropertyDao() {
    }

    @Override
    public int save(Houseproperty e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Houseproperty e) {
        session.update(e);
    }

    @Override
    public void delete(Houseproperty e) {
        session.delete(e);
    }

    @Override
    public Houseproperty get(Houseproperty e) {
        Houseproperty e1 = (Houseproperty)session.get(Houseproperty.class,e.getHousePropertyId());
      
        return e1;
    }

    @Override
    public List<Houseproperty> getAll() {
        List<Houseproperty> l = session.createCriteria(Houseproperty.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
