/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Employer;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class EmployerDao implements DaoAction<Employer>{

    public EmployerDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Employer e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Employer e) {
         session.update(e);
    }

    @Override
    public void delete(Employer e) {
          session.delete(e);
    }

    @Override
    public Employer get(Employer e) {
         Employer e1 = (Employer)session.get(Employer.class,e.getEmployerId());
      
        return e1;
    }

    @Override
    public List<Employer> getAll() {
         List<Employer> l = session.createCriteria(Employer.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
