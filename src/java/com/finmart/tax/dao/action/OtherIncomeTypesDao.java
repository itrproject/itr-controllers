/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;


import com.finmart.tax.dao.Otherincometypes;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class OtherIncomeTypesDao implements DaoAction<Otherincometypes>{

    public OtherIncomeTypesDao(Session session) {
        this.session = session;
    }

    Session session;
   
    @Override
    public int save(Otherincometypes e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Otherincometypes e) {
        session.update(e);
    }

    @Override
    public void delete(Otherincometypes e) {
         session.delete(e);
    }

    @Override
    public Otherincometypes get(Otherincometypes e) {
        Otherincometypes e1 = (Otherincometypes)session.get(Otherincometypes.class,e.getOtherIncomeTypesId());
      
        return e1;
    }

    @Override
    public List<Otherincometypes> getAll() {
        List<Otherincometypes> l = session.createCriteria(Otherincometypes.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
