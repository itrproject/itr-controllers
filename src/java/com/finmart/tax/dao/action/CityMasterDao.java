/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.CityMaster;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class CityMasterDao implements DaoAction<CityMaster>{

    public CityMasterDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(CityMaster e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(CityMaster e) {
         session.update(e);
    }

    @Override
    public void delete(CityMaster e) {
          session.delete(e);
    }

    @Override
    public CityMaster get(CityMaster e) {
         CityMaster e1 = (CityMaster)session.get(CityMaster.class,e.getCityId());
      
        return e1;
    }

    @Override
    public List<CityMaster> getAll() {
        List<CityMaster> l = session.createCriteria(CityMaster.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
