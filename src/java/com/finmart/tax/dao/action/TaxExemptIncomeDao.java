/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.hibernateUtil.NewHibernateUtil;
import com.finmart.tax.dao.Taxexemptincome;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class TaxExemptIncomeDao implements DaoAction<Taxexemptincome>{

    public TaxExemptIncomeDao(Session session) {
        this.session = session;
    }
    Session session;
    @Override
    public int save(Taxexemptincome e) {
      
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
      
        return lastId;
    }

    @Override
    public void update(Taxexemptincome e) {
     
       
        session.update(e);
      
    }

    @Override
    public void delete(Taxexemptincome e) {
     
        session.delete(e);
     
    }

    @Override
    public Taxexemptincome get(Taxexemptincome e) {
     
        Taxexemptincome e1 = (Taxexemptincome)session.get(Taxexemptincome.class,e.getTaxExemptIncomeId());
     
        return e1;
    }

    @Override
    public List<Taxexemptincome> getAll() {
     
        List<Taxexemptincome> l = session.createCriteria(Taxexemptincome.class).addOrder(Order.asc("taxExemptIncomeID")).list();
     
        return l;
    }

   
   
    
}
