/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Deductionundersectiontype;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DeductionsUnderSectionTypeDao implements DaoAction<Deductionundersectiontype>{

    public DeductionsUnderSectionTypeDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Deductionundersectiontype e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Deductionundersectiontype e) {
         session.update(e);
    }

    @Override
    public void delete(Deductionundersectiontype e) {
        session.delete(e);
    }

    @Override
    public Deductionundersectiontype get(Deductionundersectiontype e) {
         Deductionundersectiontype e1 = (Deductionundersectiontype)session.get(Deductionundersectiontype.class,e.getDeductionUnderSectionTypeId());
      
        return e1;
    }

    @Override
    public List<Deductionundersectiontype> getAll() {
         List<Deductionundersectiontype> l = session.createCriteria(Deductionundersectiontype.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
