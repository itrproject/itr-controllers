/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.CoOwners;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class CoOwnersDao implements DaoAction<CoOwners>{

    public CoOwnersDao(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(CoOwners e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(CoOwners e) {
        session.update(e);
    }

    @Override
    public void delete(CoOwners e) {
        session.delete(e);
    }

    @Override
    public CoOwners get(CoOwners e) {
        CoOwners e1 = (CoOwners)session.get(CoOwners.class,e.getCoOwnersId());
      
        return e1;
    }

    @Override
    public List<CoOwners> getAll() {
        List<CoOwners> l = session.createCriteria(CoOwners.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
