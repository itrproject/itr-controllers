/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Rentincome;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class RentIncomeDao implements DaoAction<Rentincome>{

   
    Session session;
    public RentIncomeDao(Session session) {
        this.session = session;
    }
    @Override
    public int save(Rentincome e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Rentincome e) {
         session.update(e);
    }

    @Override
    public void delete(Rentincome e) {
         session.delete(e);
    }

    @Override
    public Rentincome get(Rentincome e) {
         Rentincome e1 = (Rentincome)session.get(Rentincome.class,e.getRentIncomeId());
      
        return e1;
    }

    @Override
    public List<Rentincome> getAll() {
        List<Rentincome> l = session.createCriteria(Rentincome.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
