/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Healthinsurancepremium;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class HealthInsurancePremiumDao implements DaoAction<Healthinsurancepremium>{

    public HealthInsurancePremiumDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Healthinsurancepremium e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Healthinsurancepremium e) {
       session.update(e);
    }

    @Override
    public void delete(Healthinsurancepremium e) {
        session.delete(e);
    }

    @Override
    public Healthinsurancepremium get(Healthinsurancepremium e) {
        Healthinsurancepremium e1 = (Healthinsurancepremium)session.get(Healthinsurancepremium.class,e.getHealthInsurancePremiumId());
      
        return e1;
    }

    @Override
    public List<Healthinsurancepremium> getAll() {
        List<Healthinsurancepremium> l = session.createCriteria(Healthinsurancepremium.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
