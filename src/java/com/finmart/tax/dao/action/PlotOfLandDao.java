/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Plotofland;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class PlotOfLandDao implements DaoAction<Plotofland>{

    public PlotOfLandDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Plotofland e) {
          session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Plotofland e) {
         session.update(e);
    }

    @Override
    public void delete(Plotofland e) {
       session.delete(e);
    }

    @Override
    public Plotofland get(Plotofland e) {
         Plotofland e1 = (Plotofland)session.get(Plotofland.class,e.getPlotId());
      
        return e1;
    }

    @Override
    public List<Plotofland> getAll() {
        List<Plotofland> l = session.createCriteria(Plotofland.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
