/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.hibernateUtil.NewHibernateUtil;
import com.finmart.tax.dao.UserMaster;
import com.finmart.tax.dao.Useritmapping;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Administrator
 */
public class userITMappingDao implements DaoAction<Useritmapping>{
Session session;

    public userITMappingDao() {
    }

    public userITMappingDao(Session session) {
        this.session = session;
    }

    
    @Override
    public int save(Useritmapping e) {      
        session.save(e);
        int lastId = ((BigInteger)session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();      
        return lastId;
    }

    @Override
    public void update(Useritmapping e) {     
        session.update(e);
    }

    @Override
    public void delete(Useritmapping e) {
        session.delete(e);
    }

    @Override
    public Useritmapping get(Useritmapping e) {
        Useritmapping e1 = (Useritmapping)session.get(Useritmapping.class,e.getUitmid());        
        return e1;
    }
    
    public Useritmapping getUsingUID(int UserID){        
        Useritmapping userITMapping;
        Criteria criteria = session.createCriteria(Useritmapping.class).add(Restrictions.eq("userMaster.UId",UserID));
        List results = criteria.list();
        if(results.size()>0){
            userITMapping=(Useritmapping)results.get(0);
        }
        else
        {
            userITMapping=null;
        }        
        return userITMapping;
        
    }

    @Override
    public List<Useritmapping> getAll() {        
        List<Useritmapping> l = session.createCriteria(Useritmapping.class).addOrder(Order.asc("uitmid")).list();       
        return l;
    }

  
    
}
