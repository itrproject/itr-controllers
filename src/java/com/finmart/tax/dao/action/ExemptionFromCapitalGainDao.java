/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Exemptionfromcapitalgain;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class ExemptionFromCapitalGainDao implements DaoAction<Exemptionfromcapitalgain>{

    public ExemptionFromCapitalGainDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Exemptionfromcapitalgain e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Exemptionfromcapitalgain e) {
        session.update(e);
    }

    @Override
    public void delete(Exemptionfromcapitalgain e) {
       session.delete(e);
    }

    @Override
    public Exemptionfromcapitalgain get(Exemptionfromcapitalgain e) {
         Exemptionfromcapitalgain e1 = (Exemptionfromcapitalgain)session.get(Exemptionfromcapitalgain.class,e.getExemptionFromCapitalGainId());
      
        return e1;
    }

    @Override
    public List<Exemptionfromcapitalgain> getAll() {
        List<Exemptionfromcapitalgain> l = session.createCriteria(Exemptionfromcapitalgain.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
