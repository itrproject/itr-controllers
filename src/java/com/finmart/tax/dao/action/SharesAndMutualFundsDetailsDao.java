/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Sharesandmutualfundsdetails;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class SharesAndMutualFundsDetailsDao implements DaoAction<Sharesandmutualfundsdetails>{

    public SharesAndMutualFundsDetailsDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Sharesandmutualfundsdetails e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Sharesandmutualfundsdetails e) {
        session.update(e);
    }

    @Override
    public void delete(Sharesandmutualfundsdetails e) {
        session.delete(e);
    }

    @Override
    public Sharesandmutualfundsdetails get(Sharesandmutualfundsdetails e) {
        Sharesandmutualfundsdetails e1 = (Sharesandmutualfundsdetails)session.get(Sharesandmutualfundsdetails.class,e.getShareAndFundsId());
     
        return e1;
    }

    @Override
    public List<Sharesandmutualfundsdetails> getAll() {
        List<Sharesandmutualfundsdetails> l = session.createCriteria(Sharesandmutualfundsdetails.class).addOrder(Order.asc("UId")).list();
      
        return l;
    }
    
}
