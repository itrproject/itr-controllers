/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.hibernateUtil.NewHibernateUtil;
import com.finmart.tax.dao.Interestearned;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class InterestIncomeDao implements DaoAction<Interestearned>{

    public InterestIncomeDao(Session session) {
        this.session = session;
    }
    Session session;
    @Override
    public int save(Interestearned e) {
       
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Interestearned e) {
       
        session.update(e);
      
    }

    @Override
    public void delete(Interestearned e) {
       
        session.delete(e);
      
    }

    @Override
    public Interestearned get(Interestearned e) {
       
        Interestearned e1 = (Interestearned)session.get(Interestearned.class,e.getInterestEarnedId());
     
        return e1;
    }

    @Override
    public List<Interestearned> getAll() {
       
        List<Interestearned> l = session.createCriteria(Interestearned.class).addOrder(Order.asc("UId")).list();
      
        return l;
    }


    
}
