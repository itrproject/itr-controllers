/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Broughtforwardlosses;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class BroughtForwardLossesDao implements DaoAction<Broughtforwardlosses>{

    public BroughtForwardLossesDao(Session session) {
        this.session = session;
    }

    Session session;
    
    @Override
    public int save(Broughtforwardlosses e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Broughtforwardlosses e) {
         session.update(e);
    }

    @Override
    public void delete(Broughtforwardlosses e) {
          session.delete(e);
    }

    @Override
    public Broughtforwardlosses get(Broughtforwardlosses e) {
          Broughtforwardlosses e1 = (Broughtforwardlosses)session.get(Broughtforwardlosses.class,e.getBroughtForwardLossesId());
      
        return e1;
    }

    @Override
    public List<Broughtforwardlosses> getAll() {
        List<Broughtforwardlosses> l = session.createCriteria(Broughtforwardlosses.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
