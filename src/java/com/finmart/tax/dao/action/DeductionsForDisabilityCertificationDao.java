/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Deductionfordisabilitycertification;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DeductionsForDisabilityCertificationDao implements DaoAction<Deductionfordisabilitycertification>{

    public DeductionsForDisabilityCertificationDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Deductionfordisabilitycertification e) {
          session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Deductionfordisabilitycertification e) {
       session.update(e);
    }

    @Override
    public void delete(Deductionfordisabilitycertification e) {
         session.delete(e);
    }

    @Override
    public Deductionfordisabilitycertification get(Deductionfordisabilitycertification e) {
         Deductionfordisabilitycertification e1 = (Deductionfordisabilitycertification)session.get(Deductionfordisabilitycertification.class,e.getDeductionForDisabilityCertificationId());
      
        return e1;
    }

    @Override
    public List<Deductionfordisabilitycertification> getAll() {
         List<Deductionfordisabilitycertification> l = session.createCriteria(Deductionfordisabilitycertification.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
