/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Donations;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class DonationsDao implements DaoAction<Donations>{

    public DonationsDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Donations e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(Donations e) {
        session.update(e);
    }

    @Override
    public void delete(Donations e) {
         session.delete(e);
    }

    @Override
    public Donations get(Donations e) {
         Donations e1 = (Donations)session.get(Donations.class,e.getDonationId());
      
        return e1;
    }

    @Override
    public List<Donations> getAll() {
         List<Donations> l = session.createCriteria(Donations.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
