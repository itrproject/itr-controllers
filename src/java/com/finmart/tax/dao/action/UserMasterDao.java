/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.hibernateUtil.NewHibernateUtil;
import com.finmart.tax.dao.UserMaster;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class UserMasterDao implements DaoAction<UserMaster>{
    Session session;

    public UserMasterDao(Session session) {
        this.session = session;
    }
    
    @Override
    public int save(UserMaster e) {
        session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();
       
        return lastId;
    }

    @Override
    public void update(UserMaster e) {
      
        session.update(e);
      
    }

    @Override
    public void delete(UserMaster e) {
       
        session.delete(e);
      
    }

    @Override
    public UserMaster get(UserMaster e) {
     
        UserMaster e1 = (UserMaster)session.get(UserMaster.class,e.getUId());
       
        return e1;
    }

    @Override
    public List<UserMaster> getAll() {
     
        List<UserMaster> l = session.createCriteria(UserMaster.class).addOrder(Order.asc("UId")).list();
      
        return l;
    }

   
    
}
