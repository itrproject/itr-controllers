/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import java.util.List;

/**
 *
 * @author admin
 */
public interface DaoAction<E> {
    
    public int save (E e);
    public void update (E e);
    public void delete (E e);
    public E get (E e);
    public List<E> getAll ();
   
   
    
}
