/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Otherdeductions;
import java.math.BigInteger;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Administrator
 */
public class OtherDeductionsDao implements DaoAction<Otherdeductions>{

    public OtherDeductionsDao(Session session) {
        this.session = session;
    }

    Session session;
    @Override
    public int save(Otherdeductions e) {
         session.save(e);
        int lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).intValue();       
        return lastId;
    }

    @Override
    public void update(Otherdeductions e) {
        session.update(e);
    }

    @Override
    public void delete(Otherdeductions e) {
        session.delete(e);
    }

    @Override
    public Otherdeductions get(Otherdeductions e) {
         Otherdeductions e1 = (Otherdeductions)session.get(Otherdeductions.class,e.getOtherDeductionsId());
      
        return e1;
    }

    @Override
    public List<Otherdeductions> getAll() {
         List<Otherdeductions> l = session.createCriteria(Otherdeductions.class).addOrder(Order.asc("incomeID")).list();
       
        return l;
    }
    
}
