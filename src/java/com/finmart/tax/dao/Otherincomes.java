package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0



/**
 * Otherincomes generated by hbm2java
 */
public class Otherincomes  implements java.io.Serializable {


     private Integer otherIncomeId;
     private Otherincometypes otherincometypes;
     private Income income;
     private int amountOfIncome;

    public Otherincomes() {
    }

    public Otherincomes(Otherincometypes otherincometypes, Income income, int amountOfIncome) {
       this.otherincometypes = otherincometypes;
       this.income = income;
       this.amountOfIncome = amountOfIncome;
    }
   
    public Integer getOtherIncomeId() {
        return this.otherIncomeId;
    }
    
    public void setOtherIncomeId(Integer otherIncomeId) {
        this.otherIncomeId = otherIncomeId;
    }
    public Otherincometypes getOtherincometypes() {
        return this.otherincometypes;
    }
    
    public void setOtherincometypes(Otherincometypes otherincometypes) {
        this.otherincometypes = otherincometypes;
    }
    public Income getIncome() {
        return this.income;
    }
    
    public void setIncome(Income income) {
        this.income = income;
    }
    public int getAmountOfIncome() {
        return this.amountOfIncome;
    }
    
    public void setAmountOfIncome(int amountOfIncome) {
        this.amountOfIncome = amountOfIncome;
    }




}


