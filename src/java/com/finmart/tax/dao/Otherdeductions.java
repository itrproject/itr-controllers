package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0



/**
 * Otherdeductions generated by hbm2java
 */
public class Otherdeductions  implements java.io.Serializable {


     private Integer otherDeductionsId;
     private Deductions deductions;
     private Deductionundersectiontype deductionundersectiontype;
     private int amountOfDeductions;

    public Otherdeductions() {
    }

    public Otherdeductions(Deductions deductions, Deductionundersectiontype deductionundersectiontype, int amountOfDeductions) {
       this.deductions = deductions;
       this.deductionundersectiontype = deductionundersectiontype;
       this.amountOfDeductions = amountOfDeductions;
    }
   
    public Integer getOtherDeductionsId() {
        return this.otherDeductionsId;
    }
    
    public void setOtherDeductionsId(Integer otherDeductionsId) {
        this.otherDeductionsId = otherDeductionsId;
    }
    public Deductions getDeductions() {
        return this.deductions;
    }
    
    public void setDeductions(Deductions deductions) {
        this.deductions = deductions;
    }
    public Deductionundersectiontype getDeductionundersectiontype() {
        return this.deductionundersectiontype;
    }
    
    public void setDeductionundersectiontype(Deductionundersectiontype deductionundersectiontype) {
        this.deductionundersectiontype = deductionundersectiontype;
    }
    public int getAmountOfDeductions() {
        return this.amountOfDeductions;
    }
    
    public void setAmountOfDeductions(int amountOfDeductions) {
        this.amountOfDeductions = amountOfDeductions;
    }




}


