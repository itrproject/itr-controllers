package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0


import java.util.HashSet;
import java.util.Set;

/**
 * Allowances generated by hbm2java
 */
public class Allowances  implements java.io.Serializable {


     private Integer allwceId;
     private String allowanceType;
     private Set employerallowances = new HashSet(0);

    public Allowances() {
    }

	
    public Allowances(String allowanceType) {
        this.allowanceType = allowanceType;
    }
    public Allowances(String allowanceType, Set employerallowances) {
       this.allowanceType = allowanceType;
       this.employerallowances = employerallowances;
    }
   
    public Integer getAllwceId() {
        return this.allwceId;
    }
    
    public void setAllwceId(Integer allwceId) {
        this.allwceId = allwceId;
    }
    public String getAllowanceType() {
        return this.allowanceType;
    }
    
    public void setAllowanceType(String allowanceType) {
        this.allowanceType = allowanceType;
    }
    public Set getEmployerallowances() {
        return this.employerallowances;
    }
    
    public void setEmployerallowances(Set employerallowances) {
        this.employerallowances = employerallowances;
    }




}


