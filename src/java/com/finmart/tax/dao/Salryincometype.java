package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0


import java.util.HashSet;
import java.util.Set;

/**
 * Salryincometype generated by hbm2java
 */
public class Salryincometype  implements java.io.Serializable {


     private Integer salaryIncomeTypeId;
     private String salaryIncomeType;
     private Set salaryincomes = new HashSet(0);

    public Salryincometype() {
    }

    public Salryincometype(String salaryIncomeType, Set salaryincomes) {
       this.salaryIncomeType = salaryIncomeType;
       this.salaryincomes = salaryincomes;
    }
   
    public Integer getSalaryIncomeTypeId() {
        return this.salaryIncomeTypeId;
    }
    
    public void setSalaryIncomeTypeId(Integer salaryIncomeTypeId) {
        this.salaryIncomeTypeId = salaryIncomeTypeId;
    }
    public String getSalaryIncomeType() {
        return this.salaryIncomeType;
    }
    
    public void setSalaryIncomeType(String salaryIncomeType) {
        this.salaryIncomeType = salaryIncomeType;
    }
    public Set getSalaryincomes() {
        return this.salaryincomes;
    }
    
    public void setSalaryincomes(Set salaryincomes) {
        this.salaryincomes = salaryincomes;
    }




}


