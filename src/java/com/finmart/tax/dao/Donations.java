package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0



/**
 * Donations generated by hbm2java
 */
public class Donations  implements java.io.Serializable {


     private Integer donationId;
     private Deductions deductions;
     private Donationtypes donationtypes;
     private int donatedAmount;
     private String nameOfDonee;
     private String panofDonee;
     private String addressOfDonee;
     private int deductionAmount;

    public Donations() {
    }

    public Donations(Deductions deductions, Donationtypes donationtypes, int donatedAmount, String nameOfDonee, String panofDonee, String addressOfDonee, int deductionAmount) {
       this.deductions = deductions;
       this.donationtypes = donationtypes;
       this.donatedAmount = donatedAmount;
       this.nameOfDonee = nameOfDonee;
       this.panofDonee = panofDonee;
       this.addressOfDonee = addressOfDonee;
       this.deductionAmount = deductionAmount;
    }
   
    public Integer getDonationId() {
        return this.donationId;
    }
    
    public void setDonationId(Integer donationId) {
        this.donationId = donationId;
    }
    public Deductions getDeductions() {
        return this.deductions;
    }
    
    public void setDeductions(Deductions deductions) {
        this.deductions = deductions;
    }
    public Donationtypes getDonationtypes() {
        return this.donationtypes;
    }
    
    public void setDonationtypes(Donationtypes donationtypes) {
        this.donationtypes = donationtypes;
    }
    public int getDonatedAmount() {
        return this.donatedAmount;
    }
    
    public void setDonatedAmount(int donatedAmount) {
        this.donatedAmount = donatedAmount;
    }
    public String getNameOfDonee() {
        return this.nameOfDonee;
    }
    
    public void setNameOfDonee(String nameOfDonee) {
        this.nameOfDonee = nameOfDonee;
    }
    public String getPanofDonee() {
        return this.panofDonee;
    }
    
    public void setPanofDonee(String panofDonee) {
        this.panofDonee = panofDonee;
    }
    public String getAddressOfDonee() {
        return this.addressOfDonee;
    }
    
    public void setAddressOfDonee(String addressOfDonee) {
        this.addressOfDonee = addressOfDonee;
    }
    public int getDeductionAmount() {
        return this.deductionAmount;
    }
    
    public void setDeductionAmount(int deductionAmount) {
        this.deductionAmount = deductionAmount;
    }




}


