package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0


import java.util.HashSet;
import java.util.Set;

/**
 * Otherincometypes generated by hbm2java
 */
public class Otherincometypes  implements java.io.Serializable {


     private Integer otherIncomeTypesId;
     private String natureOfIncome;
     private Set otherincomeses = new HashSet(0);

    public Otherincometypes() {
    }

	
    public Otherincometypes(String natureOfIncome) {
        this.natureOfIncome = natureOfIncome;
    }
    public Otherincometypes(String natureOfIncome, Set otherincomeses) {
       this.natureOfIncome = natureOfIncome;
       this.otherincomeses = otherincomeses;
    }
   
    public Integer getOtherIncomeTypesId() {
        return this.otherIncomeTypesId;
    }
    
    public void setOtherIncomeTypesId(Integer otherIncomeTypesId) {
        this.otherIncomeTypesId = otherIncomeTypesId;
    }
    public String getNatureOfIncome() {
        return this.natureOfIncome;
    }
    
    public void setNatureOfIncome(String natureOfIncome) {
        this.natureOfIncome = natureOfIncome;
    }
    public Set getOtherincomeses() {
        return this.otherincomeses;
    }
    
    public void setOtherincomeses(Set otherincomeses) {
        this.otherincomeses = otherincomeses;
    }




}


