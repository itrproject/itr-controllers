package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0


import java.util.HashSet;
import java.util.Set;

/**
 * Employer generated by hbm2java
 */
public class Employer  implements java.io.Serializable {


     private Integer employerId;
     private Employertype employertype;
     private CityMaster cityMaster;
     private String employerName;
     private String employerAddress;
     private int employmentPeriod;
     private String employerTanNo;
     private Set salaryincomes = new HashSet(0);

    public Employer() {
    }

	
    public Employer(Employertype employertype, CityMaster cityMaster, String employerName, String employerAddress, int employmentPeriod, String employerTanNo) {
        this.employertype = employertype;
        this.cityMaster = cityMaster;
        this.employerName = employerName;
        this.employerAddress = employerAddress;
        this.employmentPeriod = employmentPeriod;
        this.employerTanNo = employerTanNo;
    }
    public Employer(Employertype employertype, CityMaster cityMaster, String employerName, String employerAddress, int employmentPeriod, String employerTanNo, Set salaryincomes) {
       this.employertype = employertype;
       this.cityMaster = cityMaster;
       this.employerName = employerName;
       this.employerAddress = employerAddress;
       this.employmentPeriod = employmentPeriod;
       this.employerTanNo = employerTanNo;
       this.salaryincomes = salaryincomes;
    }
   
    public Integer getEmployerId() {
        return this.employerId;
    }
    
    public void setEmployerId(Integer employerId) {
        this.employerId = employerId;
    }
    public Employertype getEmployertype() {
        return this.employertype;
    }
    
    public void setEmployertype(Employertype employertype) {
        this.employertype = employertype;
    }
    public CityMaster getCityMaster() {
        return this.cityMaster;
    }
    
    public void setCityMaster(CityMaster cityMaster) {
        this.cityMaster = cityMaster;
    }
    public String getEmployerName() {
        return this.employerName;
    }
    
    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }
    public String getEmployerAddress() {
        return this.employerAddress;
    }
    
    public void setEmployerAddress(String employerAddress) {
        this.employerAddress = employerAddress;
    }
    public int getEmploymentPeriod() {
        return this.employmentPeriod;
    }
    
    public void setEmploymentPeriod(int employmentPeriod) {
        this.employmentPeriod = employmentPeriod;
    }
    public String getEmployerTanNo() {
        return this.employerTanNo;
    }
    
    public void setEmployerTanNo(String employerTanNo) {
        this.employerTanNo = employerTanNo;
    }
    public Set getSalaryincomes() {
        return this.salaryincomes;
    }
    
    public void setSalaryincomes(Set salaryincomes) {
        this.salaryincomes = salaryincomes;
    }




}


