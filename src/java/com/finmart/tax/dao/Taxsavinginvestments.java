package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0



/**
 * Taxsavinginvestments generated by hbm2java
 */
public class Taxsavinginvestments  implements java.io.Serializable {


     private Integer taxSavingsInvestmentsId;
     private Investmenttoolstypes investmenttoolstypes;
     private Deductions deductions;
     private int investmentAmount;

    public Taxsavinginvestments() {
    }

    public Taxsavinginvestments(Investmenttoolstypes investmenttoolstypes, Deductions deductions, int investmentAmount) {
       this.investmenttoolstypes = investmenttoolstypes;
       this.deductions = deductions;
       this.investmentAmount = investmentAmount;
    }
   
    public Integer getTaxSavingsInvestmentsId() {
        return this.taxSavingsInvestmentsId;
    }
    
    public void setTaxSavingsInvestmentsId(Integer taxSavingsInvestmentsId) {
        this.taxSavingsInvestmentsId = taxSavingsInvestmentsId;
    }
    public Investmenttoolstypes getInvestmenttoolstypes() {
        return this.investmenttoolstypes;
    }
    
    public void setInvestmenttoolstypes(Investmenttoolstypes investmenttoolstypes) {
        this.investmenttoolstypes = investmenttoolstypes;
    }
    public Deductions getDeductions() {
        return this.deductions;
    }
    
    public void setDeductions(Deductions deductions) {
        this.deductions = deductions;
    }
    public int getInvestmentAmount() {
        return this.investmentAmount;
    }
    
    public void setInvestmentAmount(int investmentAmount) {
        this.investmentAmount = investmentAmount;
    }




}


