package com.finmart.tax.dao;
// Generated Jun 25, 2015 12:31:58 PM by Hibernate Tools 3.6.0


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * UserMaster generated by hbm2java
 */
public class UserMaster  implements java.io.Serializable {


     private Integer UId;
     private String firstName;
     private String lastName;
     private String email;
     private String contactNo;
     private Date birthDate;
     private String address;
     private String city;
     private int pincode;
     private String profilePic;
     private Date openDate;
     private Set invoiceMasters = new HashSet(0);
     private Set useritmappings = new HashSet(0);
     private Set docuploadMasters = new HashSet(0);
     private Set restrancTables = new HashSet(0);
     private Set reqtrancTables = new HashSet(0);
     private Set docMasters = new HashSet(0);
     private Set enqMasters = new HashSet(0);

    public UserMaster() {
    }

	
    public UserMaster(String firstName, String lastName, String email, String contactNo, Date birthDate, String address, String city, int pincode, String profilePic, Date openDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contactNo = contactNo;
        this.birthDate = birthDate;
        this.address = address;
        this.city = city;
        this.pincode = pincode;
        this.profilePic = profilePic;
        this.openDate = openDate;
    }
    public UserMaster(String firstName, String lastName, String email, String contactNo, Date birthDate, String address, String city, int pincode, String profilePic, Date openDate, Set invoiceMasters, Set useritmappings, Set docuploadMasters, Set restrancTables, Set reqtrancTables, Set docMasters, Set enqMasters) {
       this.firstName = firstName;
       this.lastName = lastName;
       this.email = email;
       this.contactNo = contactNo;
       this.birthDate = birthDate;
       this.address = address;
       this.city = city;
       this.pincode = pincode;
       this.profilePic = profilePic;
       this.openDate = openDate;
       this.invoiceMasters = invoiceMasters;
       this.useritmappings = useritmappings;
       this.docuploadMasters = docuploadMasters;
       this.restrancTables = restrancTables;
       this.reqtrancTables = reqtrancTables;
       this.docMasters = docMasters;
       this.enqMasters = enqMasters;
    }
   
    public Integer getUId() {
        return this.UId;
    }
    
    public void setUId(Integer UId) {
        this.UId = UId;
    }
    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getContactNo() {
        return this.contactNo;
    }
    
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
    public Date getBirthDate() {
        return this.birthDate;
    }
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    public int getPincode() {
        return this.pincode;
    }
    
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
    public String getProfilePic() {
        return this.profilePic;
    }
    
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }
    public Date getOpenDate() {
        return this.openDate;
    }
    
    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }
    public Set getInvoiceMasters() {
        return this.invoiceMasters;
    }
    
    public void setInvoiceMasters(Set invoiceMasters) {
        this.invoiceMasters = invoiceMasters;
    }
    public Set getUseritmappings() {
        return this.useritmappings;
    }
    
    public void setUseritmappings(Set useritmappings) {
        this.useritmappings = useritmappings;
    }
    public Set getDocuploadMasters() {
        return this.docuploadMasters;
    }
    
    public void setDocuploadMasters(Set docuploadMasters) {
        this.docuploadMasters = docuploadMasters;
    }
    public Set getRestrancTables() {
        return this.restrancTables;
    }
    
    public void setRestrancTables(Set restrancTables) {
        this.restrancTables = restrancTables;
    }
    public Set getReqtrancTables() {
        return this.reqtrancTables;
    }
    
    public void setReqtrancTables(Set reqtrancTables) {
        this.reqtrancTables = reqtrancTables;
    }
    public Set getDocMasters() {
        return this.docMasters;
    }
    
    public void setDocMasters(Set docMasters) {
        this.docMasters = docMasters;
    }
    public Set getEnqMasters() {
        return this.enqMasters;
    }
    
    public void setEnqMasters(Set enqMasters) {
        this.enqMasters = enqMasters;
    }




}


